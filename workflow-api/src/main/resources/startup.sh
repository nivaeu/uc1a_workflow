#!/bin/sh

java -jar original-${project.build.finalName}.jar db migrate config.yml && \
java -jar original-${project.build.finalName}.jar server config.yml
