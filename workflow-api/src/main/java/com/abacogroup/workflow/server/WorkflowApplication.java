package com.abacogroup.workflow.server;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static java.util.stream.Collectors.toList;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.statement.SqlLogger;
import org.jdbi.v3.core.statement.StatementContext;
import org.locationtech.jts.geom.Geometry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.abacogroup.workflow.server.api.common.WKTGeometryDeserializer;
import com.abacogroup.workflow.server.api.common.WKTGeometrySerializer;
import com.abacogroup.workflow.server.db.ProcessDao;
import com.abacogroup.workflow.server.db.WorkflowDao;
import com.abacogroup.workflow.server.db.ora.OracleProcessDao;
import com.abacogroup.workflow.server.db.ora.OracleWorkflowDao;
import com.abacogroup.workflow.server.resources.ProcessResources;
import com.abacogroup.workflow.server.services.ProcessService;
import com.abacogroup.workflow.server.services.WorkflowService;
import com.abacogroup.workflow.server.tasks.WorkflowUploadTask;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;

import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.db.PooledDataSourceFactory;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.jdbi3.bundles.JdbiExceptionsBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;
import io.swagger.v3.oas.integration.SwaggerConfiguration;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;

public class WorkflowApplication extends Application<WorkflowConfiguration> {

	private static final Logger log = LoggerFactory.getLogger(WorkflowApplication.class);

	public static void main(String[] args) throws Exception {
		new WorkflowApplication().run(args);
	}

	@Override
	public String getName() {
		return "Workflow Engine";
	}

	@Override
	public void initialize(Bootstrap<WorkflowConfiguration> bootstrap) {
		bootstrap.addBundle(new JdbiExceptionsBundle());
		bootstrap.addBundle(new MigrationsBundle<WorkflowConfiguration>() {
			@Override
			public PooledDataSourceFactory getDataSourceFactory(WorkflowConfiguration configuration) {
				return configuration.getDataSourceFactory();
			}
		});

		// bootstrap.addCommand(new ShutdownCommand());

		SubstitutingSourceProvider provider = new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(), new EnvironmentVariableSubstitutor(false));
		bootstrap.setConfigurationSourceProvider(provider);
	}

	@Override
	public void run(WorkflowConfiguration config, Environment environment) throws Exception {
		ObjectMapper objectMapper = environment.getObjectMapper();
		configure(objectMapper);

		// environment.admin().addTask(new ShutdownTask());

		final JdbiFactory factory = new JdbiFactory();
		final Jdbi jdbi = factory.build(environment, config.getDataSourceFactory(), "workflowDB");
		if (config.isEnableSqlLogging()) {
			SqlLogger sqlLogger = new SqlLogger() {
				@Override
				public void logAfterExecution(StatementContext context) {
					log.info("sql {}, parameters {}, timeTaken {} ms", context.getRenderedSql(), context.getBinding().toString(), context.getElapsedTime(ChronoUnit.MILLIS));
				}
			};
			jdbi.setSqlLogger(sqlLogger);
		}
		
		File workflowsPath = initializeWorkflowsDirectory(config);
		File dynamicWorkflowPath = initializeDynamicWorkflowsDirectory(config);
		File libsPath = initializeWorkflowSharedDependenciesDirectory(config);
		
		// Resources
		ProcessDao processDao;
		WorkflowDao workflowDao;
		if (config.isOracle()) {
			processDao = jdbi.onDemand(OracleProcessDao.class);
			workflowDao = jdbi.onDemand(OracleWorkflowDao.class);		
		} else {
			processDao = jdbi.onDemand(ProcessDao.class);
			workflowDao = jdbi.onDemand(WorkflowDao.class);
		}
		
		WorkflowService workflowService = new WorkflowService(processDao, workflowDao, getAdditionalWorkflowLibraries(libsPath), environment.getObjectMapper());

		environment.admin().addTask(new WorkflowUploadTask(workflowService, dynamicWorkflowPath));

		loadWorkflows(workflowService, workflowsPath, dynamicWorkflowPath);

		
		ProcessService processService = new ProcessService(processDao, workflowDao, environment.getObjectMapper(), workflowService);	
		environment.jersey().register(new ProcessResources(processService));
		initOpenApi(environment);
	}

	private List<URL> getAdditionalWorkflowLibraries(File libsPath)
	{
		if (libsPath == null)
			return new ArrayList<>(0);
		
		File[] files = libsPath.listFiles((dir, name) -> name.toLowerCase().endsWith(".jar"));
		if (files == null)
			return new ArrayList<>(0);
		
		return Arrays.stream(files)
			.map(f -> {
				try
				{
					return f.toURI().toURL();
				}
				catch (MalformedURLException e)
				{
					throw new RuntimeException(e);
				}
			})
			.collect(toList());
	}

	private File initializeWorkflowSharedDependenciesDirectory(WorkflowConfiguration config)
	{
		String path = config.getWorkflowSharedDependenciesPath();
		if (StringUtils.isEmpty(path))
			return null;
		
		File file = new File(path);		
		if (!file.canRead() || !file.isDirectory())
			throw new IllegalStateException("workflowSharedDependenciesPath is not a directory or it is not readable");
		
		return file;
	}

	private File initializeWorkflowsDirectory(WorkflowConfiguration config)
	{
		File workflowsPath = new File(config.getWorkflowPath());
		if (!workflowsPath.canRead() || !workflowsPath.isDirectory())
			throw new IllegalStateException("workflowPath is not a directory");
		return workflowsPath;
	}

	private File initializeDynamicWorkflowsDirectory(WorkflowConfiguration config)
	{
		File dynamicWorkflowPath = new File(config.getDynamicWorkflowPath());
		if (dynamicWorkflowPath.exists() && !dynamicWorkflowPath.isDirectory())
			throw new IllegalStateException("dynamicWorkflowPath is not a directory");
		else if (!dynamicWorkflowPath.exists())
			createDynamicWfFolder(dynamicWorkflowPath);
		
		initializeDynamicWfTemporaryFolder(dynamicWorkflowPath);
		return dynamicWorkflowPath;
	}
	
	private void createDynamicWfFolder(File dynamicWorkflowPath) {
		if (!dynamicWorkflowPath.mkdirs())
			throw new RuntimeException("Error creating dynamic workflow directory");		
	}

	private void initializeDynamicWfTemporaryFolder(File dynamicWorkflowPath)
	{
		File tmp = new File(dynamicWorkflowPath, WorkflowService.DYNAMIC_WF_TEMPORARY_DIR_SUFFIX);
		if (!tmp.exists() && !tmp.mkdir())
			throw new IllegalStateException("Cannot create directory: " + tmp.getAbsolutePath());

		if (!tmp.isDirectory())
			throw new IllegalStateException(tmp.getAbsolutePath() + " is not a directory");

		Arrays.stream(tmp.listFiles()).forEach(f -> {
			if (f.isFile()) 
				f.delete();
		});
	}

	private void configure(ObjectMapper objectMapper) {
		objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		
		SimpleModule module = new SimpleModule("custom-deserializers", Version.unknownVersion());
		module.addSerializer(Geometry.class, new WKTGeometrySerializer());
		module.addDeserializer(Geometry.class, new WKTGeometryDeserializer());
		objectMapper.registerModule(module);
	}

	private void initOpenApi(Environment environment) {
		OpenAPI oas = new OpenAPI();

		oas.info(new Info().title("ABACO Workflow engine").contact(new Contact().email("info@abacogroup.eu")));

		SwaggerConfiguration oasConfig = new SwaggerConfiguration().openAPI(oas).prettyPrint(true).resourcePackages(Stream.of("com.abacogroup.workflow.server.resources").collect(Collectors.toSet()));

		environment.jersey().register(new OpenApiResource().openApiConfiguration(oasConfig));
	}
	
	private void loadWorkflows(WorkflowService workflowService, File workflowsPath, File dynamicWorkflowPath) throws Exception {
			
		// SCAN WF JAR FILES FORM CONFIG PATH
		if (!workflowsPath.isDirectory()) {
			throw new RuntimeException("Directory workflow error");
		}
		
		File[] files = workflowsPath.listFiles();
		for (File wfJar : files) {
			if (wfJar.isFile()) 
				workflowService.loadWorkflow(wfJar);
		}
		
		for(File wfJar : dynamicWorkflowPath.listFiles()) {
			if(wfJar.isFile()) 
				workflowService.loadWorkflow(wfJar, dynamicWorkflowPath);
			
		}

		// TODO: Check that all workflows in the db have a classloader
	}

}
