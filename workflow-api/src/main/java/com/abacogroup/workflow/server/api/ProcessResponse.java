package com.abacogroup.workflow.server.api;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.time.Instant;
import java.util.List;
import java.util.Map;

import com.abacogroup.workflow.sdk.beans.WorkflowMessage;
import com.abacogroup.workflow.server.db.ntt.WorLogTransition;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Data
@Builder
public class ProcessResponse
{
	private Long process_id;
	private String current_node_name;
	private Map<String, Object> current_node_metadata;
	private Log transition_log;
	private List<WorkflowMessage> messages;
	private Map<String, Object> global_vars;

	@Getter
	public static class Log {
		private Long id;
		private Integer transition_id;
		private Instant start_date;
		private Instant end_date;
		private String user_id;
		private boolean failed;
		
		public Log(WorLogTransition ntt) {
			id = ntt.getId();
			transition_id = ntt.getWor_workflow_transition_id();
			start_date = ntt.getStart_date().toInstant();
			end_date = ntt.getEnd_date().toInstant();
			user_id = ntt.getUser_id();
			failed = ntt.getIs_failed() != 0;
		}
	}
	
}
