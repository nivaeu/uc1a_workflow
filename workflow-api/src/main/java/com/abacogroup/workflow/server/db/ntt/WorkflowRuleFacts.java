package com.abacogroup.workflow.server.db.ntt;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkflowRuleFacts {
	@NonNull  private Integer id;

	@NonNull  private String fact_name;
	
	@NonNull  private String fact_type;
	
	@NonNull  private Integer is_list;

	@NonNull private Integer wor_workflow_node_id;
	
}
