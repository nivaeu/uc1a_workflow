package com.abacogroup.workflow.server.db.ntt;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.sql.Timestamp;

import lombok.Data;

@Data
public class WorProcess {
	
	private Long id;
	private String node_current;
	private Timestamp start_date;
	private Integer wor_workflow_id;
	private String code;
}
