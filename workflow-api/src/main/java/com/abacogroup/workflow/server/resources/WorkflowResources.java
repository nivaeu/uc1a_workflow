package com.abacogroup.workflow.server.resources;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.abacogroup.workflow.server.db.WorkflowDao;
import com.abacogroup.workflow.server.db.ntt.Workflow;
import com.codahale.metrics.annotation.Timed;

@Path("/api/v1/workflow")
@Timed
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class WorkflowResources
{
	private WorkflowDao workflowDao;
	
	public WorkflowResources(WorkflowDao workflowDao) {
		this.workflowDao = workflowDao;
	}

	@GET
	public Optional<Workflow> getWorkflow(@Valid @NotNull Long workflowId)
	{
		return workflowDao.getWorkflow(workflowId);
	}
	

}
