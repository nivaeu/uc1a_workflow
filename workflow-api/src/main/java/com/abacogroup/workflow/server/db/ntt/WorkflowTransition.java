package com.abacogroup.workflow.server.db.ntt;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WorkflowTransition {	

	private Integer id;

	@NonNull private Integer node_from;

	@NonNull private Integer node_to;
	
	private Integer is_default;
	
	private String description;
	
	@NonNull private Integer wor_workflow_id;
	
}
