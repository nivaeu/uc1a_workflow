package com.abacogroup.workflow.server.db;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;
import org.jdbi.v3.sqlobject.transaction.Transactional;

import com.abacogroup.workflow.server.api.ProcessDataBean;
import com.abacogroup.workflow.server.db.ntt.WorLogTransition;
import com.abacogroup.workflow.server.db.ntt.WorLogTransitionWithDetails;
import com.abacogroup.workflow.server.db.ntt.WorProcess;
import com.abacogroup.workflow.server.db.ntt.WorProcessData;

public interface ProcessDao extends Transactional<ProcessDao> {

	@SqlQuery("select nextval('wor_processes_seq')")
	public Long nextSequenceValue();

	@SqlQuery("select nextval('wor_logs_seq')")
	public Long nextLogSequenceValue();

	@SqlUpdate("INSERT INTO wor_process_data (id, variable, value, wor_process_id) VALUES (:id, :variable, :value, :wor_process_id)")
	public void insertProcessData(@BindBean WorProcessData wpd);

	@SqlUpdate("INSERT INTO wor_process (id, node_current, wor_workflow_id) VALUES (:id, :node_current, :wor_workflow_id)")
	public void insertProcess(@Bind("id") Long processId, @Bind("wor_workflow_id") Integer workflowId, @Bind("node_current") String currentNode);

	@Transaction
	default Long createProcess(Integer workflowId, String currentNodeName, List<WorProcessData> processData) {
		Long processId = nextSequenceValue();
		insertProcess(processId, workflowId, currentNodeName);
		processData.forEach(wpd -> {
			wpd.setId(nextSequenceValue());
			wpd.setWor_process_id(processId);
			insertProcessData(wpd);
		});

		return processId;
	}

	@SqlUpdate("UPDATE wor_process SET node_current=:node_current WHERE id=:process_id")
	public void setCurrentNode(@Bind("process_id") long process_id, @Bind("node_current") String node_current);

	@SqlUpdate("UPDATE wor_log_transition SET end_date=:end_date, is_failed=:is_failed, messages=:messages WHERE id=:id")
	public void updateLogTransition(@Bind("id") Long transitionLofId,@Bind("end_date") Timestamp endDate, @Bind("is_failed") int isFailes, @Bind("messages") String messages);

	@Transaction
	default Long createLogTransition(ProcessDataBean processDataBean, Timestamp transitionStart, Integer transitionId) {		
		long wlogtransId = nextLogSequenceValue();
		useHandle(h -> {
			h.createUpdate("INSERT INTO wor_log_transition (id, start_date, user_id, wor_process_id, wor_workflow_transition_id)"
				+ " VALUES (:id, :start_date, :user_id, :wor_process_id, :wor_workflow_transition_id)")
				.bind("id", wlogtransId)
				.bind("start_date", transitionStart)
				.bind("user_id", processDataBean.getUserId())
				.bind("wor_process_id", processDataBean.getProcessId())
				.bind("wor_workflow_transition_id", transitionId)
				.execute();
		});
		
		return wlogtransId;
	}

	@SqlQuery("SELECT DISTINCT wor_process.node_current FROM wor_process INNER JOIN wor_workflow "
		+ "ON wor_process.wor_workflow_id = wor_workflow.id  WHERE wor_workflow.code = :code")
	public List<String> findProcessCurrentNodeByWorkflowCode(@Bind("code") String code);

	@RegisterBeanMapper(WorProcess.class)
	@SqlQuery("SELECT WP.*, WF.code FROM wor_workflow_process WP left join wor_workflow WF"
		+ " on WF.id = WP.wor_workflow_id WHERE WP.id:process_id AND CURRENT_TIMESTAMP BETWEEN WF.dt_insert AND WF.dt_delete")
	public Optional<WorProcess> findProcessByProcessIdAndTime(@Bind("workflow_id") Long process_id);

	@RegisterBeanMapper(WorProcess.class)
	@SqlQuery("select wor_process.*, wor_workflow.code from wor_process INNER JOIN wor_workflow ON  "
		+ "wor_process.wor_workflow_id = wor_workflow.id where wor_process.id=:process_id")
	public Optional<WorProcess> findProcess(@Bind("process_id") Long process_id);

	@RegisterBeanMapper(WorProcessData.class)
	@SqlQuery("select * from wor_process_data where wor_process_id=:process_id")
	public List<WorProcessData> findProcessData(@Bind("process_id") Long process_id);

	@RegisterBeanMapper(WorLogTransition.class)
	@SqlQuery("SELECT * FROM wor_log_transition WHERE wor_process_id=:process_id ORDER BY id DESC")
	public List<WorLogTransition> findLogTransitionsByProcessId(@Bind("process_id") Long process_id);

	@RegisterBeanMapper(WorLogTransitionWithDetails.class)
	@SqlQuery("SELECT lt.*, wt.description, nf.name as node_from, nt.name as node_to"
		+ " FROM wor_log_transition lt"
		+ "     INNER JOIN wor_workflow_transition wt ON wt.id = lt.wor_workflow_transition_id"
		+ "     INNER JOIN wor_workflow_node nf ON nf.id = wt.node_from"
		+ "     INNER JOIN wor_workflow_node nt ON nt.id = wt.node_to"
		+ " WHERE lt.wor_process_id=:process_id"
		+ " ORDER BY lt.start_date DESC")
	public List<WorLogTransitionWithDetails> findLogTransitionsWithDetailsByProcessId(@Bind("process_id") Long process_id);

	@RegisterBeanMapper(WorLogTransition.class)
	@SqlQuery("SELECT * FROM wor_log_transition WHERE id=:id")
	public WorLogTransition findLogTransitionById(@Bind("id") Long id);

}
