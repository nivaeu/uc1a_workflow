package com.abacogroup.workflow.server.db.ntt;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.sql.Timestamp;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class WorkflowNode
{
	@NotNull
	Integer id;

	private String action;

	@NotNull
	private String type;

	@NotNull
	private Integer wor_workflow_id;

	@NotNull
	private String name;

	private String metadata;

	@NotNull
	private Timestamp dt_insert;

	private Timestamp dt_delete;

	public WorkflowNode(Integer id, String action, @NotNull String type, @NotNull Integer wor_workflow_id, @NotNull String name, String metadata, Timestamp dt_insert)
	{
		this.id = id;
		this.action = action;
		this.type = type;
		this.wor_workflow_id = wor_workflow_id;
		this.name = name;
		this.metadata = metadata;
		this.dt_insert = dt_insert;
	}

}
