package com.abacogroup.workflow.server.services;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static java.util.stream.Collectors.toList;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.abacogroup.workflow.drools.service.DroolsService;
import com.abacogroup.workflow.drools.service.FireRulesRequest;
import com.abacogroup.workflow.sdk.beans.WorkflowMessage;
import com.abacogroup.workflow.sdk.proc.Procedure;
import com.abacogroup.workflow.server.api.CreateProcessRequest;
import com.abacogroup.workflow.server.api.NodeType;
import com.abacogroup.workflow.server.api.ProcessDataBean;
import com.abacogroup.workflow.server.api.ProcessResponse;
import com.abacogroup.workflow.server.api.ProcessResponse.Log;
import com.abacogroup.workflow.server.api.TransitionRequest;
import com.abacogroup.workflow.server.api.TransitionsHistoryResponse;
import com.abacogroup.workflow.server.api.TransitionsResponse;
import com.abacogroup.workflow.server.db.ProcessDao;
import com.abacogroup.workflow.server.db.WorkflowDao;
import com.abacogroup.workflow.server.db.ntt.WorLogTransition;
import com.abacogroup.workflow.server.db.ntt.WorLogTransitionWithDetails;
import com.abacogroup.workflow.server.db.ntt.WorProcess;
import com.abacogroup.workflow.server.db.ntt.WorProcessData;
import com.abacogroup.workflow.server.db.ntt.Workflow;
import com.abacogroup.workflow.server.db.ntt.WorkflowNode;
import com.abacogroup.workflow.server.db.ntt.WorkflowRuleFacts;
import com.abacogroup.workflow.server.db.ntt.WorkflowTransition;
import com.abacogroup.workflow.server.db.ntt.WorkflowTransitionRule;
import com.abacogroup.workflow.server.db.ntt.WorkflowTransitionWithScope;
import com.abacogroup.workflow.server.db.ntt.WorkflowVariable;
import com.abacogroup.workflow.server.services.classloader.WorkflowClassLoader;
import com.abacogroup.workflow.server.services.scripting.TransitionRuleScriptSupport;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import groovy.lang.Binding;
import groovy.lang.Script;

public class ProcessService {

	private static final int MAX_NUMBER_OF_STEPS = 100;
	
	private WorkflowDao workflowDao;
	private ProcessDao processDao;
	private ObjectMapper objectMapper;
	private static final Logger log = LoggerFactory.getLogger(ProcessService.class);
	private WorkflowService workflowService;

	public ProcessService(ProcessDao processDao, WorkflowDao workflowDao, ObjectMapper objectMapper, WorkflowService workflowService) throws Exception {
		super();
		this.processDao = processDao;
		this.workflowDao = workflowDao;
		this.objectMapper = objectMapper;
		this.workflowService = workflowService;
	}

	public ProcessResponse createProcess(CreateProcessRequest request) throws Exception {
		log.debug("createProcess on workflow code {} ", request.getWorkflow_code());

		Timestamp now = new Timestamp(System.currentTimeMillis());
		WorkflowClassLoader classLoader = workflowService.getClassLoaderForWorkflow(request.getWorkflow_code());

		ProcessDataBean processDataBean = null;
		
		Set<String> globalVarsToPersist = new HashSet<>();
		Long processId;
		try {
			Integer workflowId = workflowDao.findWorkflowByCode(request.getWorkflow_code())
				.map(Workflow::getId)
				.orElseThrow(() -> new NotFoundException("Workflow definition not found"));

			WorkflowNode workflowNode = workflowDao.getWorkflowNodeByType(workflowId, NodeType.start.toString())
				.orElseThrow(() -> new NotFoundException("Workflow node start not found"));

			List<WorkflowVariable> workflowVarList = getRequiredVariables(request, workflowId);
			Map<String, Object> globalVars = prepareGlobalVars(request, classLoader, globalVarsToPersist, workflowVarList);

			List<WorProcessData> processData = globalVarsToPersist.stream()
				.map(name -> new WorProcessData(null, writeValueAsString(globalVars.get(name)), name, null))
				.collect(toList());

			processId = processDao.createProcess(workflowId, workflowNode.getName(), processData);

			processDataBean = ProcessDataBean.builder()
				.workflowCode(request.getWorkflow_code())
				.userId("userId")								// TODO: auth
				.scope(request.getScope())
				.globalVars(globalVars)
				.workflowId(workflowId)
				.processId(processId)
				.currentNode(workflowNode)
				.startTime(now)
				.workflowClassLoader(classLoader)
				.build();

		} catch (WebApplicationException ex) {
			throw ex;
		} catch (Exception e) {
			String message = String.format("Exception while creating process for workflow Id %s : %s", processDataBean.getWorkflowId(), e.getLocalizedMessage());
			log.error(message, e);
			throw new WebApplicationException("Process creation failed", Response.Status.INTERNAL_SERVER_ERROR);
		}

		WorkflowTransition firstTransition = getNextTransition(processDataBean);
		WorLogTransition worLogTransition = doTransition(firstTransition, processDataBean);

		Map<String, Object> metadata = deserializeMetadata(processDataBean.getCurrentNode().getMetadata());

		return ProcessResponse.builder()
			.current_node_name(processDataBean.getCurrentNode().getName())
			.current_node_metadata(metadata)
			.process_id(processId)
			.transition_log(new Log(worLogTransition))
			.messages(processDataBean.getWorkflowMessages())
			.global_vars(request.isReturn_global_vars() ?  processDataBean.getGlobalVars() : null)
			.build();
	}

	private String writeValueAsString(Object obj) {
		try {
			return objectMapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	private Map<String, Object> prepareGlobalVars(CreateProcessRequest request, ClassLoader classLoader, Set<String> varNamesToPersist, List<WorkflowVariable> workflowVarList) {
		Map<String, Object> globalVars = new HashMap<>();
		TypeFactory typeFactory = TypeFactory.defaultInstance().withClassLoader(classLoader);
		
		WorkflowVariable workflowVar;
		for (Map.Entry<String, ?> entry : request.getGlobal_vars().entrySet()) {
			workflowVar = workflowVarList.stream()
				.filter(var -> entry.getKey().equals(var.getVar_name()))
				.findAny()
				.orElseThrow(() -> new BadRequestException("Workflow input variable definition not found"));
			
			JavaType jt = typeFactory.constructFromCanonical(workflowVar.getVar_type());
			Object objVar;
			if (workflowVar.getIs_list() == 0) {
				objVar = objectMapper.convertValue(entry.getValue(), jt);
			} else {
				objVar = objectMapper.convertValue(entry.getValue(), typeFactory.constructCollectionType(List.class, jt));
			}

			globalVars.put(entry.getKey(), objVar);

			if (workflowVar.getFl_persist() != 0)
				varNamesToPersist.add(entry.getKey());
		}

		return globalVars;
	}

	private List<WorkflowVariable> getRequiredVariables(CreateProcessRequest request, Integer workflowId) {
		List<WorkflowVariable> workflowVarList = workflowDao.getWorkflowVars(workflowId);
		for (WorkflowVariable workflowVar : workflowVarList) {
			if (request.getGlobal_vars().get(workflowVar.getVar_name()) == null) {
				throw new BadRequestException(String.format("Missing request global variable : %s", workflowVar.getVar_name()));
			}
		}

		return workflowVarList;
	}

	private Map<String, Object> deserializeMetadata(String strMetadata) throws JsonProcessingException, JsonMappingException {
		if (StringUtils.isEmpty(strMetadata))
			return null;

		return objectMapper.readValue(strMetadata, new TypeReference<Map<String, Object>>() {});
	}

	private WorLogTransition doTransition(WorkflowTransition transition, ProcessDataBean processData) throws Exception {
		log.debug("doTransition starting from {} on process {}", transition.getDescription(), processData.getProcessId());
		
		boolean keepGoing = true;
		boolean isError = false;
		
		WorkflowNode nodeInit = processData.getCurrentNode();
		
		int loops = 0;
		
		do {
			Timestamp transitionStart = new Timestamp(System.currentTimeMillis());
			try {
				WorkflowNode currentNode = workflowDao.getWorkflowNodeById(transition.getNode_to())
					.orElseThrow(() -> new NotFoundException("Workflow next node not found"));

				Long idLogTransition = processDao.createLogTransition(processData, transitionStart, transition.getId());
				
				processData.setCurrentNode(currentNode);
				processData.setLogTransitionId(idLogTransition);

				NodeType nodeType = NodeType.valueOf(currentNode.getType());
				
				switch (nodeType) {
				case start:
					break;
				case status:
					keepGoing = false;
					break;
				case rule:
					execDroolsNode(currentNode, processData, processData.getWorkflowMessages());
					break;
				case proc:
					execProcNode(currentNode.getAction(), processData, processData.getWorkflowMessages());
					break;
				case end:
					keepGoing = false;
					break;
				default:
					throw new IllegalStateException("Unknown node type");
				}

				if (++loops > MAX_NUMBER_OF_STEPS)
					throw new IllegalStateException("ABORTING: too many transitions performed " + loops + ", possible infinite loop.");
				
				if (keepGoing) {
					transition = getNextTransition(processData);
				}
			} catch (Exception e) {
				String message = String.format("Exception while executing transition '%s': %s", transition.getDescription(), e.getLocalizedMessage());
				log.error(message, e);
				processData.getWorkflowMessages().add(new WorkflowMessage("error", message, false));
				isError = true;
			}
			
			String jsonMessages = objectMapper.writeValueAsString(processData.getWorkflowMessages());
			Timestamp endDate = new Timestamp(System.currentTimeMillis());
			processDao.updateLogTransition(processData.getLogTransitionId(), endDate, isError ? 1 : 0, jsonMessages);

		} while (keepGoing && !isError);

		// We reset the processData current node to the initial value because we didn't move forward in case of error
		if (isError)
			processData.setCurrentNode(nodeInit);
		else
			processDao.setCurrentNode(processData.getProcessId(), processData.getCurrentNode().getName());
		
		return processDao.findLogTransitionById(processData.getLogTransitionId());
	}

	private void execProcNode(String className, ProcessDataBean processData, List<WorkflowMessage> workflowMessages) throws Exception {
		log.debug("execProcNode {} on process {}", className, processData.getProcessId());

		if (className.endsWith(".groovy")) {
			execGroovyProcedure(processData.getWorkflowClassLoader(), className, processData, workflowMessages);
		} else {
			Class<?> clazz = processData.getWorkflowClassLoader().loadClass(className);
			execJavaProcedure(clazz, processData, workflowMessages);
		}
	}

	private void execGroovyProcedure(WorkflowClassLoader classLoader, String className, ProcessDataBean processData, List<WorkflowMessage> workflowMessages) throws Exception {
		Class<Procedure> clazz = classLoader.getGroovyProcedureClass(className);
		execJavaProcedure(clazz, processData, workflowMessages);
	}

	private void execJavaProcedure(Class<?> clazz, ProcessDataBean processData, List<WorkflowMessage> workflowMessages) throws Exception	{
		if (!Procedure.class.isAssignableFrom(clazz))
			throw new IllegalStateException(clazz.getName() + " is not an instance of Procedure");
		
		@SuppressWarnings("unchecked")
		Procedure objProc = ((Class<Procedure>) clazz).getDeclaredConstructor().newInstance();
		objProc.execute(processData);
	}

	private void execDroolsNode(WorkflowNode nodeRule, ProcessDataBean processData, List<WorkflowMessage> workflowMessages) throws Exception {
		log.debug("execDroolsNode {} on process {}", nodeRule.getName(), processData.getProcessId());

		String filePath = nodeRule.getAction();

		DroolsService droolsService = DroolsService.create(processData.getWorkflowClassLoader());

		// EXTRACT VARIABLE NAMES FROM RULE FILE
		Map<String, Object> globalsMap = droolsService.getGlobalsMap(filePath, processData.getGlobalVars());

		globalsMap.put("workflowMessages", workflowMessages);

		List<Object> factList = new ArrayList<Object>();

		List<WorkflowRuleFacts> facts = workflowDao.getRuleFacts(nodeRule.getId());

		// GET RULE FACTS FROM DB
		for (WorkflowRuleFacts f : facts) {
			// GET FACT FROM GLOBALS BY NAME
			Object factObj = processData.getGlobalVars().get(f.getFact_name());
			if (factObj != null) {
				if (f.getIs_list() != null && f.getIs_list() == 1) {
					factList.addAll((List<?>) factObj);
				} else {
					factList.add(factObj);
				}
			} else {
				log.warn("expected fact not found executing node rule {} : {}", nodeRule.getName(), f.getFact_name());
			}
		}

		FireRulesRequest frr = new FireRulesRequest(filePath, factList, globalsMap);
		droolsService.fireRules(frr);
	}

	private boolean isTransitionInScope(WorkflowTransitionWithScope transition, String scope) {
		if (transition.getScope() == null || transition.getScope().equalsIgnoreCase(scope)) {
			return true;
		}
		return false;
	}

	private WorkflowTransition getNextTransition(ProcessDataBean processData) throws Exception {
		log.debug("getNextTransition on process {} from node '{}'", processData.getProcessId(), processData.getCurrentNode().getName());

		WorkflowNode workflowNode = workflowDao.getWorkflowNodeByName(processData.getWorkflowId(), processData.getCurrentNode().getName(), processData.getStartTime())
			.orElseThrow(() -> new NotFoundException("Workflow node not found: " + processData.getCurrentNode().getName()));

		List<WorkflowTransitionWithScope> availableTransitions = workflowDao.getWorkflowTransitions(processData.getWorkflowId(), workflowNode.getId(), processData.getStartTime())
			.stream()
			.filter(ts -> isTransitionInScope(ts, processData.getScope()))
			.collect(toList());

		if (availableTransitions.isEmpty())
			throw new NotFoundException("Next workflow transition not found");
		
		WorkflowTransition transition = null;
		WorkflowTransition transitionDefault = null;
		for (WorkflowTransitionWithScope t : availableTransitions) {
			if (t.getIs_default() != null && t.getIs_default() == 1)
				transitionDefault = t;
			
			List<WorkflowTransitionRule> transitionRules = workflowDao.getWorkflowTransitionRules(t.getId());
			if (evaluateTransitionRules(transitionRules, processData)) {
				transition = t;
				break;
			}
		}
		
		if (transition != null)
			return transition;
		
		if (transitionDefault != null)
			return transitionDefault;
		
		/* 
		 * At workflow load time, we chack that if any rules is set on the transitions
		 * a default transition must be set as well, so if we only have one possible transition
		 * we can assume that no rules are in place and we can safely execute it.
		 */
		if (availableTransitions.size() == 1)
			return availableTransitions.get(0);
		
		throw new NotFoundException("No workflow transition has been found with valid rules or default");
	}

	private boolean evaluateTransitionRules(List<WorkflowTransitionRule> transitionRules, ProcessDataBean processData) throws Exception {
		for (WorkflowTransitionRule transitionRule : transitionRules) {
			Binding binding = new Binding(processData.getGlobalVars());
			Script script = TransitionRuleScriptSupport.createScript(binding, transitionRule.getExpression());

			Object obj = script.run();
			if (obj == null)
				throw new IllegalStateException("Transition rule expression evaluated to null: " + transitionRule.getExpression());

			boolean res = true;
			if (obj instanceof Boolean)
				res = (Boolean) obj;
			else if (Boolean.TYPE.isAssignableFrom(obj.getClass()))
				res = (boolean) obj;
			else
				throw new IllegalStateException("Transition rule expression evaluated to class: " + obj.getClass());

			if (!res)
				return false;
		}

		return true;
	}

	public TransitionsResponse getUserAvailableTransitions(Long processId, String scope) throws Exception {
		log.debug("getAvailableTransitions for process {} on scope '{}'", scope);

		Timestamp now = new Timestamp(System.currentTimeMillis());
		WorProcess wp = processDao.findProcess(processId).orElseThrow(() -> new NotFoundException("Workflow process not found"));

		WorkflowNode workflowNode = workflowDao.getWorkflowNodeByName(wp.getWor_workflow_id(), wp.getNode_current(), now)
			.orElseThrow(() -> new NotFoundException("Workflow node not found"));

		List<WorkflowTransitionWithScope> wftList = workflowDao.getWorkflowTransitions(wp.getWor_workflow_id(), workflowNode.getId(), now);

		Iterator<WorkflowTransitionWithScope> transitionIter = wftList.iterator();
		while (transitionIter.hasNext()) {// FILTER ONLY TRANSITIONS IN SCOPE
			if (!isTransitionInScope(transitionIter.next(), scope)) {
				transitionIter.remove();
			}
		}

		TransitionsResponse workflowAllowedTransitions = new TransitionsResponse(wftList);
		return workflowAllowedTransitions;
	}

	public ProcessResponse executeProcessTransition(Long processId, Integer transitionId, TransitionRequest request, String userId) throws Exception {
		log.debug("setProcessTransition '{}' on process {} ", transitionId, processId);
		
		try {
			WorProcess worProcess = processDao.findProcess(processId).orElseThrow(() -> new NotFoundException("Workflow process not found"));

			Timestamp now = new Timestamp(System.currentTimeMillis());
			WorkflowClassLoader classLoader = workflowService.getClassLoaderForWorkflow(worProcess.getCode());

			WorkflowTransition transition = workflowDao.getWorkflowTransition(transitionId).orElseThrow(() -> new NotFoundException("Workflow process transition not found"));

			WorkflowNode workflowNode = workflowDao.getWorkflowNodeByName(worProcess.getWor_workflow_id(), worProcess.getNode_current(), now)
				.orElseThrow(() -> new NotFoundException("Current node status not found"));

			if (!workflowNode.getId().equals(transition.getNode_from())) {
				throw new BadRequestException("Transition not allowed from workflow status");
			}

			HashMap<String, Object> globalVars = initializeGlobalVars(processId, worProcess, classLoader);

			ProcessDataBean processData = ProcessDataBean.builder()
				.globalVars(globalVars)
				.workflowCode(worProcess.getCode())
				.workflowId(transition.getWor_workflow_id())
				.processId(processId)
				.userId(userId)									// TODO: auth
				.scope(request.getScope())
				.currentNode(workflowNode)
				.startTime(now)
				.workflowClassLoader(classLoader)
				.build();

			WorLogTransition worLogTransition = doTransition(transition, processData);
			
			Map<String, Object> metadata = deserializeMetadata(processData.getCurrentNode().getMetadata());
			
			return ProcessResponse.builder()
				.current_node_name(processData.getCurrentNode().getName())
				.current_node_metadata(metadata)
				.global_vars(request.isReturn_global_vars() ? processData.getGlobalVars() : null)
				.process_id(processId)
				.transition_log(new Log(worLogTransition))
				.messages(processData.getWorkflowMessages())
				.build();
			
		} catch (WebApplicationException ex) {
			throw ex;
		} catch (Exception e) {
			String message = String.format("Exception while performing transition %s on processId %s : %s", transitionId, processId, e.getLocalizedMessage());
			throw new RuntimeException(message, e);
		}
	}

	private HashMap<String, Object> initializeGlobalVars(Long processId, WorProcess worProcess, WorkflowClassLoader classLoader) throws JsonProcessingException, JsonMappingException {
		List<WorkflowVariable> workflowVarList = workflowDao.getWorkflowVars(worProcess.getWor_workflow_id());
		List<WorProcessData> processDataList = processDao.findProcessData(processId);

		TypeFactory typeFactory = TypeFactory.defaultInstance().withClassLoader(classLoader);
		
		HashMap<String, Object> globalMap = new HashMap<String, Object>();
		for (WorProcessData worProcessData : processDataList) {
			WorkflowVariable workflowVar = workflowVarList.stream()
				.filter(var -> worProcessData.getVariable().equals(var.getVar_name()))
				.findAny()
				.orElseThrow(() -> new RuntimeException(String.format("Workflow variable definition not found : %s", worProcessData.getVariable())));

			JavaType jt = typeFactory.constructFromCanonical(workflowVar.getVar_type());

			Object objVar;
			if (workflowVar.getIs_list() == 1) {
				objVar = objectMapper.readValue(worProcessData.getValue(), typeFactory.constructCollectionType(List.class, jt));
			} else {
				objVar = objectMapper.readValue(worProcessData.getValue(), jt);
			}

			globalMap.put(worProcessData.getVariable(), objVar);
		}

		return globalMap;
	}

	public TransitionsHistoryResponse getTransitionsHistory(Long processId) throws Exception {
		log.debug("getTransitionsHistory for process {}", processId);

		processDao.findProcess(processId).orElseThrow(() -> new NotFoundException("Workflow process not found"));
		List<WorLogTransitionWithDetails> transitions = processDao.findLogTransitionsWithDetailsByProcessId(processId);
		return new TransitionsHistoryResponse(transitions, objectMapper);
	}

}
