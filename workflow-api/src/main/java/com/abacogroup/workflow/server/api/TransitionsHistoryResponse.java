package com.abacogroup.workflow.server.api;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static java.util.stream.Collectors.toList;

import java.sql.Timestamp;
import java.util.List;

import com.abacogroup.workflow.sdk.beans.WorkflowMessage;
import com.abacogroup.workflow.server.db.ntt.WorLogTransitionWithDetails;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;

@Getter
public class TransitionsHistoryResponse {

	private List<TransitionDetails> transitions;

	public TransitionsHistoryResponse(List<WorLogTransitionWithDetails> transitions, ObjectMapper mapper)
	{
		this.transitions = transitions.stream()
			.map(t -> new TransitionDetails(t, mapper))
			.collect(toList());
	}

	@Getter
	public static class TransitionDetails {
		private Long id;
		private Timestamp end_date;
		private Timestamp start_date;
		private String user_id;
		private Long wor_process_id;
		private Integer wor_workflow_transition_id;
		private boolean is_failed;
		private String description;
		private String node_from;
		private String node_to;
		private List<WorkflowMessage> messages;
		
		public TransitionDetails(WorLogTransitionWithDetails ntt, ObjectMapper mapper) {
			id = ntt.getId();
			end_date = ntt.getEnd_date();
			start_date = ntt.getStart_date();
			user_id = ntt.getUser_id();
			wor_process_id = ntt.getWor_process_id();
			wor_workflow_transition_id = ntt.getWor_workflow_transition_id();
			is_failed = ntt.getIs_failed() != 0;
			description = ntt.getDescription();
			node_from = ntt.getNode_from();
			node_to = ntt.getNode_to();
			try
			{
				messages = mapper.readValue(ntt.getMessages(), new TypeReference<List<WorkflowMessage>>() { });
			}
			catch (JsonProcessingException e)
			{
				throw new RuntimeException(e);
			}
		}
	}
}
