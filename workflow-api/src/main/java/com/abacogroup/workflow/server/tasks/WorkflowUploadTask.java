package com.abacogroup.workflow.server.tasks;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.abacogroup.workflow.server.services.WorkflowService;

import io.dropwizard.servlets.tasks.PostBodyTask;

public class WorkflowUploadTask extends PostBodyTask {
	
	private WorkflowService workflowService;
	private File dynamicWorkflowPath;

	public WorkflowUploadTask(WorkflowService workflowService, File dynamicWorkflowPath) {
		super("upload-workflow");
		this.workflowService = workflowService;
		this.dynamicWorkflowPath = dynamicWorkflowPath;
	}

	@Override
	public void execute(Map<String, List<String>> parameters, String body, PrintWriter output) throws Exception {
		
		String fileName = parameters.get("fileName").get(0);
		validateFileName(fileName);		

		byte[] decodedFile = Base64.getDecoder().decode(body);
		File jarFile = Files.write(Paths.get(dynamicWorkflowPath.getAbsolutePath(), fileName), decodedFile).toFile();
		
		checkFileExistWhereExpected(dynamicWorkflowPath, fileName);
		
		workflowService.loadWorkflow(jarFile, dynamicWorkflowPath);					
	}
	
	private void validateFileName(String fileName) throws Exception {
		
		Pattern regExpPattern = Pattern.compile("^[a-zA-Z0-9-_]*\\.(jar)$");
		Matcher regExpMatcher = regExpPattern.matcher(fileName);
		
		if(!regExpMatcher.matches()) {
			throw new RuntimeException("Malformed filename: " + fileName);
		}
	}
	
	private void checkFileExistWhereExpected(File folderAfterWrite, String fileName) throws Exception {
		boolean fileWrote = false;
		
		for(File file : folderAfterWrite.listFiles()) {
			if(file.getName().equals(fileName)) {
				fileWrote = true;
				break;
			}
		}
		
		if(!fileWrote) {
			throw new RuntimeException("Workflow jar not saved in the expected directory: " + dynamicWorkflowPath.getAbsolutePath() + ". Filename: " + fileName);
		}
		
	}
}
