package com.abacogroup.workflow.server.api.common;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.io.IOException;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.WKTWriter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class WKTGeometrySerializer extends StdSerializer<Geometry>
{
	private static final long serialVersionUID = -195010080189831836L;
	private static final WKTWriter WRITER = new WKTWriter();

	public WKTGeometrySerializer()
	{
		super(Geometry.class);
	}

	@Override
	public void serialize(Geometry value, JsonGenerator gen, SerializerProvider provider) throws IOException
	{
		gen.writeString(WRITER.write(value));
	}

}
