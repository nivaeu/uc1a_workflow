package com.abacogroup.workflow.server.db;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;
import org.jdbi.v3.sqlobject.transaction.Transactional;

import com.abacogroup.workflow.server.db.ntt.Workflow;
import com.abacogroup.workflow.server.db.ntt.WorkflowLog;
import com.abacogroup.workflow.server.db.ntt.WorkflowNode;
import com.abacogroup.workflow.server.db.ntt.WorkflowRuleFacts;
import com.abacogroup.workflow.server.db.ntt.WorkflowTransition;
import com.abacogroup.workflow.server.db.ntt.WorkflowTransitionRule;
import com.abacogroup.workflow.server.db.ntt.WorkflowTransitionScope;
import com.abacogroup.workflow.server.db.ntt.WorkflowTransitionWithScope;
import com.abacogroup.workflow.server.db.ntt.WorkflowVariable;

public interface WorkflowDao extends Transactional<WorkflowDao> {

	@Transaction
	default void endDateWholeWorkflow(Workflow wf, Timestamp oneSecondAgo) {
		updateWorkFlow(wf);
		updateTransitionEndDate(wf.getId(), oneSecondAgo);
		updateNodeEndDate(wf.getId(), oneSecondAgo);
		updateVariableEndDate(wf.getId(), oneSecondAgo);
	}

	@SqlQuery("select nextval('wor_definitions_seq')")
	public Integer nextSequenceValue();
	
	@SqlUpdate("UPDATE wor_workflow_variable SET dt_delete = :cur_time WHERE wor_workflow_id = :wor_workflow_id AND CURRENT_TIMESTAMP BETWEEN dt_insert AND dt_delete")
	public void updateVariableEndDate(@Bind("wor_workflow_id") Integer wor_workflow_id, @Bind("cur_time") Timestamp cur_time);

	@SqlUpdate("UPDATE wor_workflow_node SET dt_delete = :cur_time WHERE wor_workflow_id = :wor_workflow_id AND CURRENT_TIMESTAMP BETWEEN dt_insert AND dt_delete")
	public void updateNodeEndDate(@Bind("wor_workflow_id") Integer wor_workflow_id, @Bind("cur_time") Timestamp cur_time);

	@SqlUpdate("UPDATE wor_workflow_transition SET dt_delete = :cur_time WHERE wor_workflow_id = :wor_workflow_id AND CURRENT_TIMESTAMP BETWEEN dt_insert AND dt_delete")
	public void updateTransitionEndDate(@Bind("wor_workflow_id") Integer wor_workflow_id, @Bind("cur_time") Timestamp cur_time);

	@SqlUpdate("INSERT INTO wor_workflow_node (id, type, action, wor_workflow_id, name, dt_insert, metadata) VALUES (:id, :type, :action, :wor_workflow_id, :name, :dt_insert, :metadata)")
	public void insertNode(@BindBean WorkflowNode wfn);

	@SqlUpdate("INSERT INTO wor_workflow (id, code, hash_md5, version) VALUES (:id, :code, :hash_md5, :version)")
	public void insertWorkFlow(@BindBean Workflow wf);

	@SqlUpdate("UPDATE wor_workflow SET hash_md5 = :hash_md5, version = :version WHERE code = :code")
	public void updateWorkFlow(@BindBean Workflow wf);

	@SqlUpdate("INSERT INTO wor_workflow_rule_facts (id, fact_name, wor_workflow_node_id, fact_type, is_list) VALUES (:id, :fact_name, :wor_workflow_node_id, :fact_type, :is_list)")
	public void insertFact(@BindBean WorkflowRuleFacts ruleFact);

	@SqlUpdate("INSERT INTO wor_workflow_variable (id, dt_insert, var_name, var_type, wor_workflow_id, is_list, fl_persist) VALUES (:id, :dt_insert, :var_name, :var_type, :wor_workflow_id, :is_list, :fl_persist)")
	public void insertVariable(@BindBean WorkflowVariable wfv);

	@SqlUpdate("INSERT INTO wor_workflow_transition (id, node_from, node_to, description, is_default, wor_workflow_id) VALUES (:id, :node_from, :node_to, :description, :is_default, :wor_workflow_id)")
	public void insertTransition(@BindBean WorkflowTransition workflowTransition);

	@SqlUpdate("INSERT INTO wor_workflow_transition_scope (id, scope, wor_workflow_transition_id) VALUES (:id, :scope, :wor_workflow_transition_id)")
	public void insertTransitionScope(@BindBean WorkflowTransitionScope workflowTransitionScope);

	@SqlUpdate("INSERT INTO wor_workflow_transition_rule (id, expression, wor_workflow_transition_id) VALUES (:id, :expression, :wor_workflow_transition_id)")
	public void insertTransitionRule(@BindBean WorkflowTransitionRule workflowTransitionRule);

	@SqlQuery("SELECT id FROM wor_workflow WHERE id=:id")
	public Optional<Integer> findWorkflow(@Bind("id") Integer id);

	@SqlQuery("SELECT * FROM wor_workflow WHERE code=:code AND CURRENT_TIMESTAMP BETWEEN dt_insert AND dt_delete")
	@RegisterBeanMapper(Workflow.class)
	public Optional<Workflow> findWorkflowByCode(@Bind("code") String code);

	@SqlQuery("SELECT * FROM wor_workflow WHERE id=:id")
	@RegisterBeanMapper(Workflow.class)
	public Optional<Workflow> getWorkflow(@Bind("id") Long id);

	@SqlQuery("SELECT * FROM wor_workflow_node WHERE wor_workflow_id=:workflow_id AND type=:node_type AND CURRENT_TIMESTAMP BETWEEN dt_insert AND dt_delete")
	@RegisterBeanMapper(WorkflowNode.class)
	public Optional<WorkflowNode> getWorkflowNodeByType(@Bind("workflow_id") Integer workflow_id, @Bind("node_type") String node_type);

	@SqlQuery("SELECT * FROM wor_workflow_node WHERE wor_workflow_id=:workflow_id AND name=:node_name AND :ref_dt BETWEEN dt_insert AND dt_delete")
	@RegisterBeanMapper(WorkflowNode.class)
	public Optional<WorkflowNode> getWorkflowNodeByName(@Bind("workflow_id") Integer workflow_id, @Bind("node_name") String node_name, @Bind("ref_dt") Timestamp refDate);

	@SqlQuery("SELECT * FROM wor_workflow_node WHERE wor_workflow_id=:workflow_id AND type='rule' AND CURRENT_TIMESTAMP BETWEEN dt_insert AND dt_delete") 
	@RegisterBeanMapper(WorkflowNode.class)
	public List<WorkflowNode> getWorkflowRuleNodesByWorkflowId(@Bind("workflow_id") Integer workflow_id);

	@SqlQuery("SELECT * FROM wor_workflow_node WHERE wor_workflow_id=:workflow_id AND :cur_time BETWEEN dt_insert AND dt_delete")
	@RegisterBeanMapper(WorkflowNode.class)
	public List<WorkflowNode> getWorkflowNodesByWorkflowIdAndTime(@Bind("workflow_id") Integer workflow_id, @Bind("cur_time") Timestamp cur_time);

	@SqlQuery("SELECT * FROM wor_workflow_node WHERE id=:node_id")
	@RegisterBeanMapper(WorkflowNode.class)
	public Optional<WorkflowNode> getWorkflowNodeById(@Bind("node_id") Integer node_id);

	@SqlQuery("SELECT WT.*, WTS.scope FROM wor_workflow_transition WT"
		+ " left join wor_workflow_transition_scope WTS on WTS.wor_workflow_transition_id = WT.id"
		+ " WHERE WT.node_FROM=:node_FROM AND WT.wor_workflow_id=:workflow_id AND :ref_dt BETWEEN WT.dt_insert AND WT.dt_delete"
		+ " ORDER BY WT.id")
	@RegisterBeanMapper(WorkflowTransitionWithScope.class)
	public List<WorkflowTransitionWithScope> getWorkflowTransitions(@Bind("workflow_id") Integer workflow_id, @Bind("node_FROM") Integer node_from, @Bind("ref_dt") Timestamp refDate);

	@SqlQuery("SELECT * FROM wor_workflow_transition WHERE wor_workflow_id=:workflow_id AND CURRENT_TIMESTAMP BETWEEN dt_insert AND dt_delete")
	@RegisterBeanMapper(WorkflowTransition.class)
	public List<WorkflowTransition> getWorkflowTransitionByWorkflowId(@Bind("workflow_id") Integer workflow_id);

	@SqlQuery("SELECT * FROM wor_workflow_transition WHERE id=:transition_id AND CURRENT_TIMESTAMP BETWEEN dt_insert AND dt_delete")
	@RegisterBeanMapper(WorkflowTransition.class)
	public Optional<WorkflowTransition> getWorkflowTransition(@Bind("transition_id") Integer transition_id);

	@SqlQuery("SELECT * FROM wor_workflow_transition_rule WHERE wor_workflow_transition_id=:trans_id")
	@RegisterBeanMapper(WorkflowTransitionRule.class)
	public List<WorkflowTransitionRule> getWorkflowTransitionRules(@Bind("trans_id") Integer trans_id);

	@SqlQuery("SELECT * FROM wor_workflow_rule_facts WHERE wor_workflow_node_id=:node_id")
	@RegisterBeanMapper(WorkflowRuleFacts.class)
	public List<WorkflowRuleFacts> getRuleFacts(@Bind("node_id") Integer node_id);

	@RegisterBeanMapper(WorkflowVariable.class)
	@SqlQuery("SELECT * FROM wor_workflow_variable WHERE wor_workflow_id=:workflow_id AND CURRENT_TIMESTAMP BETWEEN dt_insert AND dt_delete")
	public List<WorkflowVariable> getWorkflowVars(@Bind("workflow_id") Integer workflow_id);

	@SqlUpdate("INSERT INTO wor_workflow_log (id, dt_insert, code, version, json) VALUES (:id, :dt_insert, :code, :version, :json)")
	public void insertWorkflowLog(@BindBean WorkflowLog workflowLog);

	@SqlUpdate("UPDATE wor_workflow_log SET dt_delete = :cur_time WHERE code = :code AND CURRENT_TIMESTAMP BETWEEN dt_insert AND dt_delete")
	public void endDateWorkflowLog(@Bind("code") String code, @Bind("cur_time") Timestamp cur_time);

}

