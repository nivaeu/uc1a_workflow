package com.abacogroup.workflow.server.api;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.abacogroup.workflow.sdk.beans.ProcessData;
import com.abacogroup.workflow.sdk.beans.WorkflowMessage;
import com.abacogroup.workflow.server.db.ntt.WorkflowNode;
import com.abacogroup.workflow.server.services.classloader.WorkflowClassLoader;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
public class ProcessDataBean implements ProcessData {

	private Integer workflowId;

	private String workflowCode;

	private Long processId;

	private String userId;

	private String scope;

	@NonNull
	private Timestamp startTime;

	@NonNull
	private WorkflowClassLoader workflowClassLoader;
	
	@Builder.Default
	private Map<String, ? super Object> globalVars = new HashMap<>();

	@Builder.Default
	private List<WorkflowMessage> workflowMessages = new ArrayList<>();

	private Long logTransitionId;

	private WorkflowNode currentNode;

	@Override
	public String getCurrentNodeName()
	{
		return currentNode.getName();
	}

}
