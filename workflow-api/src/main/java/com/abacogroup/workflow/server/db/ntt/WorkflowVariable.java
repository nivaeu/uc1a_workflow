package com.abacogroup.workflow.server.db.ntt;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.sql.Timestamp;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WorkflowVariable {

	@NotNull
	private Integer id;
	@NotNull 
	private Timestamp dt_insert;
	@NotNull
	private String var_name;
	@NotNull
	private String var_type;
	@NotNull
	private Integer wor_workflow_id;
	@NotNull
	private Integer is_list;
	@NotNull
	private Integer fl_persist;

}
