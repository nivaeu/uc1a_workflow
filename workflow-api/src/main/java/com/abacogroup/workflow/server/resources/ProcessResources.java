package com.abacogroup.workflow.server.resources;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.abacogroup.workflow.server.api.CreateProcessRequest;
import com.abacogroup.workflow.server.api.ProcessResponse;
import com.abacogroup.workflow.server.api.TransitionRequest;
import com.abacogroup.workflow.server.api.TransitionsHistoryResponse;
import com.abacogroup.workflow.server.api.TransitionsResponse;
import com.abacogroup.workflow.server.services.ProcessService;
import com.codahale.metrics.annotation.Timed;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@Path("/api/v1/processes")
@Timed
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProcessResources {

	private ProcessService processService;

	public ProcessResources(ProcessService processService) {
		this.processService = processService;
	}

	@POST
	@ApiResponse(responseCode = "200", description = "Process created")
	@Operation(description = "Create a new process")
	public ProcessResponse createProcess(@Valid @NotNull CreateProcessRequest request) throws Exception {
		ProcessResponse response = processService.createProcess(request);
		if (response.getTransition_log().isFailed())
			throw new WebApplicationException(Response.serverError().entity(response).build());
		
		return response;
	}

	@GET
	@Path("{processid}/transitions")
	@ApiResponse(responseCode = "200", description = "Available transitions returned")
	@Operation(description = "Get process transition list")
	public TransitionsResponse getAvailableTransitions(@NotNull @PathParam("processid") Long processId, @NotNull @QueryParam("scope") String scope) throws Exception {

		return processService.getUserAvailableTransitions(processId, scope);

	}

	@PUT
	@Path("{processid}/transitions/{transitionId}")
	@ApiResponse(responseCode = "200", description = "Process transition executed")
	@Operation(description = "Execute a process transition")
	public ProcessResponse executeProcessTransition(@NotNull @PathParam("processid") Long processId, 
		@NotNull @PathParam("transitionId") Integer transitionId, 
		@Valid @NotNull TransitionRequest request) throws Exception {
		return processService.executeProcessTransition(processId, transitionId, request, "userId");
	}
	
	@GET
	@Path("{processid}/transitions-history")
	@ApiResponse(responseCode = "200", description = "workflow process transitions-history")
	@Operation(description = "Get process transitions history")
	public TransitionsHistoryResponse getTransitionsHistory(@NotNull @PathParam("processid") Long processId) throws Exception {

		return processService.getTransitionsHistory(processId);
	}
	
}
