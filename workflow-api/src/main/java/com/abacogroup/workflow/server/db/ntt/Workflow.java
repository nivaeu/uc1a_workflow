package com.abacogroup.workflow.server.db.ntt;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Workflow
{
	@NotNull private Integer id;
	@NotNull private String code;
	@NotNull private String hash_md5;
	@NotNull private String version;
}
