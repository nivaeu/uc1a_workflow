package com.abacogroup.workflow.server.api.common;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Utils {
	
	public static String stringify(Object obj) {
		if (obj == null)
			return null;
		if (obj instanceof Date) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			return sdf.format(obj);
		}
		if (obj instanceof OffsetDateTime) {
			obj = ((OffsetDateTime) obj).toLocalDateTime();
		}
		if (obj instanceof LocalDateTime) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
			LocalDateTime dateTime = (LocalDateTime) obj;
			return dateTime.format(formatter);
		}

		return obj.toString();
	}
}
