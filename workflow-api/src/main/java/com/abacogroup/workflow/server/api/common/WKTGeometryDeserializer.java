package com.abacogroup.workflow.server.api.common;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.io.IOException;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class WKTGeometryDeserializer extends StdDeserializer<Geometry>
{
	private static final long serialVersionUID = -195010080189831836L;
	private static final WKTReader READER = new WKTReader();

	public WKTGeometryDeserializer()
	{
		super(Geometry.class);
	}

	@Override
	public Geometry deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException
	{
		String wkt = p.readValueAs(String.class);
		try
		{
			return READER.read(wkt);
		}
		catch (ParseException e)
		{
			throw new IOException(e);
		}
	}

}
