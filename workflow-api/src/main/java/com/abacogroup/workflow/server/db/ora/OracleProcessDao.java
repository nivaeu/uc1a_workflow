package com.abacogroup.workflow.server.db.ora;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import org.jdbi.v3.sqlobject.statement.SqlQuery;

import com.abacogroup.workflow.server.db.ProcessDao;

public interface OracleProcessDao extends ProcessDao {

	@SqlQuery("select wor_processes_seq.nextval from dual")
	public Long nextSequenceValue();

	@SqlQuery("select wor_logs_seq.nextval from dual")
	public Long nextLogSequenceValue();
	
}
