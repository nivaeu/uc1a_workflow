package com.abacogroup.workflow.server;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

public class WorkflowConfiguration extends Configuration {

	@Valid
	@NotNull
	private DataSourceFactory database;

	private String workflowSharedDependenciesPath;
	
	@NotEmpty
	private String workflowPath;
	
	@NotEmpty
	private String dynamicWorkflowPath;

	@NotNull
	private boolean enableSqlLogging;
	
	private boolean oracle = false;

	@JsonProperty("database")
	public void setDataSourceFactory(DataSourceFactory factory) {
		this.database = factory;
	}

	@JsonProperty("database")
	public DataSourceFactory getDataSourceFactory() {
		return database;
	}

	public boolean isEnableSqlLogging() {
		return enableSqlLogging;
	}

	public void setEnableSqlLogging(boolean enableSqlLogging) {
		this.enableSqlLogging = enableSqlLogging;
	}

	public String getWorkflowPath() {
		return workflowPath;
	}

	public void setWorkflowPath(String workflowPath) {
		this.workflowPath = workflowPath;
	}

	public String getDynamicWorkflowPath() {
		return dynamicWorkflowPath;
	}

	public void setDynamicWorkflowPath(String dynamicWorkflowPath) {
		this.dynamicWorkflowPath = dynamicWorkflowPath;
	}

	public boolean isOracle()
	{
		return oracle;
	}

	public void setOracle(boolean oracle)
	{
		this.oracle = oracle;
	}

	public String getWorkflowSharedDependenciesPath()
	{
		return workflowSharedDependenciesPath;
	}

	public void setWorkflowSharedDependenciesPath(String workflowSharedDependenciesPath)
	{
		this.workflowSharedDependenciesPath = workflowSharedDependenciesPath;
	}
	
}
