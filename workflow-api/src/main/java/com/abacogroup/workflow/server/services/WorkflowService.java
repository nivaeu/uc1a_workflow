package com.abacogroup.workflow.server.services;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.NotFoundException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.abacogroup.workflow.server.api.NodeType;
import com.abacogroup.workflow.server.api.WorkflowDto;
import com.abacogroup.workflow.server.api.WorkflowDto.Fact;
import com.abacogroup.workflow.server.api.WorkflowDto.Node;
import com.abacogroup.workflow.server.api.WorkflowDto.Procedure;
import com.abacogroup.workflow.server.api.WorkflowDto.Rule;
import com.abacogroup.workflow.server.api.WorkflowDto.Scope;
import com.abacogroup.workflow.server.api.WorkflowDto.Transition;
import com.abacogroup.workflow.server.api.WorkflowDto.Variable;
import com.abacogroup.workflow.server.db.ProcessDao;
import com.abacogroup.workflow.server.db.WorkflowDao;
import com.abacogroup.workflow.server.db.ntt.Workflow;
import com.abacogroup.workflow.server.db.ntt.WorkflowLog;
import com.abacogroup.workflow.server.db.ntt.WorkflowNode;
import com.abacogroup.workflow.server.db.ntt.WorkflowRuleFacts;
import com.abacogroup.workflow.server.db.ntt.WorkflowTransition;
import com.abacogroup.workflow.server.db.ntt.WorkflowTransitionRule;
import com.abacogroup.workflow.server.db.ntt.WorkflowTransitionScope;
import com.abacogroup.workflow.server.db.ntt.WorkflowVariable;
import com.abacogroup.workflow.server.services.classloader.WorkflowClassLoader;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion.VersionFlag;
import com.networknt.schema.ValidationMessage;

public class WorkflowService {

	public static final String DYNAMIC_WF_TEMPORARY_DIR_SUFFIX = "tmp-for-classloader";
	
	private static final String WORKFLOW_JSON_RESOURCE_NAME = "abaco-workflow.json";
	private static final Logger log = LoggerFactory.getLogger(WorkflowService.class);
	
	private ProcessDao processDao;
	private WorkflowDao workflowDao;
	private List<URL> additionalLibs;
	private ObjectMapper objectMapper;
	private ConcurrentHashMap<String, WorkflowClassLoader> classLoaderMap = new ConcurrentHashMap<>();
	
	public WorkflowService(ProcessDao processDao, WorkflowDao workflowDao, List<URL> additionalLibs, ObjectMapper objectMapper) {
		this.processDao = processDao;
		this.workflowDao = workflowDao;
		this.additionalLibs = additionalLibs;
		this.objectMapper = objectMapper;
	}

	public WorkflowClassLoader getClassLoaderForWorkflow(String workflowCode) {
		return classLoaderMap.get(workflowCode);
	}

	public void loadWorkflow(File wfJar) throws Exception {
		loadWorkflow(wfJar, null);
	}
	
	public synchronized void loadWorkflow(File wfJar, File dynamicWfpath) throws Exception {
		WorkflowClassLoader jarClassLoader = createClassLoader(wfJar, dynamicWfpath);
		
		try (InputStream jsonStream = jarClassLoader.getResourceAsStream(WORKFLOW_JSON_RESOURCE_NAME)) {
			if (jsonStream == null)
				throw new RuntimeException(String.format("Missing abaco-workflow.json in workflow jar file : %s", wfJar.getPath()));

			String workflowDefinition = IOUtils.toString(jsonStream, StandardCharsets.UTF_8.name());
			String md5 = DigestUtils.md5Hex(workflowDefinition);
			jsonSchemaValidate(workflowDefinition, wfJar.getPath());
			
			WorkflowDto workflowDto = objectMapper.readValue(workflowDefinition, WorkflowDto.class);
			validateWorkflow(workflowDto);
			
			checkThatOldAndNewWorkflowDynamicness(workflowDto.getCode(), jarClassLoader);
			
			boolean isNew = true;
			Workflow workflow = workflowDao.findWorkflowByCode(workflowDto.getCode())
				.orElse(null);
			
			boolean isUpdated = false;
			if (workflow != null) {
				isNew = false;
				isUpdated = isWorkflowUpdated(md5, workflowDto, workflow);
			}
			
			workflowDto.setMd5(md5);
			
			if (isNew || isUpdated)
				persistWorkflow(workflowDto, workflowDefinition, isNew);
			
			WorkflowClassLoader oldClassLoader = classLoaderMap.put(workflowDto.getCode(), jarClassLoader);
			if (oldClassLoader != null)
				oldClassLoader.close();
			
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void checkThatOldAndNewWorkflowDynamicness(String code, WorkflowClassLoader newClassLoader)
	{
		WorkflowClassLoader oldClassLoader = classLoaderMap.get(code);
		if (oldClassLoader == null)
			return;
		
		if (oldClassLoader.isDynamic() != newClassLoader.isDynamic()) {
			throw new UnsupportedOperationException(String.format("Cannot replace workflow %s with dynamic=%s using a new version with dynamic=%s", 
				code, oldClassLoader.isDynamic(), newClassLoader.isDynamic()));
		}
	}

	private boolean isWorkflowUpdated(String md5, WorkflowDto workflowDto, Workflow workflow)
	{
		boolean isUpdatedVersion = !workflowDto.getVersion().equals(workflow.getVersion());
		boolean isUpdatedJson = !md5.equals(workflow.getHash_md5());
		if (isUpdatedVersion != isUpdatedJson) {
			throw new RuntimeException(String.format("Error loading workflow %s : incompatible json file or workflow version", workflowDto.getCode()));
		}
		return isUpdatedVersion;
	}

	private WorkflowClassLoader createClassLoader(File wfJar, File dynamicWfpath) throws MalformedURLException, IOException
	{
		boolean isDynamic = dynamicWfpath != null;
		URL url = isDynamic ? copyDynamicWfInTmpForClassloader(wfJar, dynamicWfpath) : wfJar.toURI().toURL();
		
		List<URL> classLoaderUrls = new ArrayList<>(additionalLibs);
		classLoaderUrls.add(url);
		
		return new WorkflowClassLoader(classLoaderUrls.toArray(new URL[0]), isDynamic, getClass().getClassLoader());
	}

	public Integer persistWorkflow(WorkflowDto workflowDto, String wfJson, boolean isNew) {
		Timestamp now = new Timestamp(System.currentTimeMillis());
		Timestamp oneMillisAgo = new Timestamp(now.getTime() - 1);
		
		return workflowDao.inTransaction(dao -> {
			Workflow workflow = new Workflow(null, workflowDto.getCode(), workflowDto.getMd5(), workflowDto.getVersion());
			String message = "created";
			if (!isNew) {
				message = "updated";
				Integer workflowId = workflowDao.findWorkflowByCode(workflowDto.getCode())
					.map(Workflow::getId)
					.orElseThrow(() -> new NotFoundException("Workflow definition not found"));
				
				workflow.setId(workflowId);
				workflowDao.endDateWholeWorkflow(workflow, oneMillisAgo);
			} else {
				workflow.setId(workflowDao.nextSequenceValue());
				workflowDao.insertWorkFlow(workflow);
			}
			
			workflowDao.endDateWorkflowLog(workflowDto.getCode(), oneMillisAgo);
			
			WorkflowLog workflowLog = new WorkflowLog(workflowDao.nextSequenceValue(), now, workflowDto.getCode(), workflowDto.getVersion(), wfJson);
			workflowDao.insertWorkflowLog(workflowLog);
			
			persistWorkflowVariables(workflowDto, workflow.getId(), now);
			persistWorkflowNodes(workflowDto, workflow.getId(), now);
			persistWorkflowTransitions(workflowDto, workflow.getId(), now);
			
			log.debug("Workflow {} has been {}", workflowDto.getCode(), message);
			
			return workflow.getId();
		});
	}

	private void persistWorkflowVariables(WorkflowDto workflowDto, Integer workflowId, Timestamp now)
	{
		for (Variable v : workflowDto.getVariables()) {
			workflowDao.insertVariable(WorkflowVariable.builder()
				.dt_insert(now)
				.fl_persist(v.getFl_persist())
				.id(workflowDao.nextSequenceValue())
				.is_list(v.getIs_list())
				.var_name(v.getName())
				.var_type(v.getType())
				.wor_workflow_id(workflowId)
				.build());
		}
	}

	private void persistWorkflowTransitions(WorkflowDto workflowDto, Integer workflowId, Timestamp now)
	{
		Map<String, WorkflowNode> nodes = workflowDao.getWorkflowNodesByWorkflowIdAndTime(workflowId, now)
			.stream()
			.collect(toMap(WorkflowNode::getName, node -> node));
		
		workflowDto.getTransitions().forEach(transition -> {
			int nodeFrom = nodes.get(transition.getNode_from()).getId();
			int nodeTo = nodes.get(transition.getNode_to()).getId();
			WorkflowTransition workflowTransition = WorkflowTransition.builder()
				.id(workflowDao.nextSequenceValue())
				.node_from(nodeFrom)
				.node_to(nodeTo)
				.is_default(transition.getIs_default())
				.description(transition.getDescription())
				.wor_workflow_id(workflowId)
				.build();
			
			workflowDao.insertTransition(workflowTransition);
			Integer transitionId = workflowTransition.getId();
			if (transition.getTran_rules() != null) {
				transition.getTran_rules().forEach(tranRuleObi -> {
					WorkflowTransitionRule workflowTransitionRule = WorkflowTransitionRule.builder()
						.id(workflowDao.nextSequenceValue())
						.expression(tranRuleObi.getExpression())
						.wor_workflow_transition_id(transitionId)
						.build();
					
					workflowDao.insertTransitionRule(workflowTransitionRule);
				});
			}
			
			if (transition.getScopes() != null) {
				transition.getScopes().forEach(s -> {
					workflowDao.insertTransitionScope(new WorkflowTransitionScope(workflowDao.nextSequenceValue(), s.getScope(), transitionId));
				});
			}
		});
	}

	private void persistWorkflowNodes(WorkflowDto workflowDto, Integer workflowId, Timestamp now)
	{
		Map<String, Rule> rules = workflowDto.getRules().stream().collect(Collectors.toMap(Rule::getNode_name, rule -> rule));
		Map<String, Procedure> procs = workflowDto.getProcedures().stream().collect(Collectors.toMap(Procedure::getNode_name, proc -> proc));
		
		workflowDto.getNodes().forEach(nodeObi -> {
			Rule rule = null;
			String action = "";
			switch (nodeObi.getType()) {
			case rule:
				rule = rules.get(nodeObi.getName());
				action = rule.getDrools_file();
				break;
			case proc:
				action = procs.get(nodeObi.getName()).getClass_name();
				break;
			default:
				break;
			}
			
			String metadata = null;
			if (nodeObi.getMetadata() != null) {
				try {
					metadata = objectMapper.writeValueAsString(nodeObi.getMetadata());
				} catch (JsonProcessingException e) {
					log.error("metadata has not be parsed in node {}, for workflow {}", nodeObi.getName(), workflowDto.getCode());
				}
			}
			
			WorkflowNode node = new WorkflowNode(workflowDao.nextSequenceValue(), action, nodeObi.getType().toString(), workflowId, nodeObi.getName(), metadata, now);
			Integer node_id = node.getId();
			workflowDao.insertNode(node);
			
			if (rule != null) {
				for (Fact fact : rule.getFacts()) {
					WorkflowRuleFacts ruleFact = new WorkflowRuleFacts(workflowDao.nextSequenceValue(), fact.getFact_name(), fact.getFact_type(), fact.getIs_list(), node_id);
					workflowDao.insertFact(ruleFact);
				}
			}
		});
	}

	public void validateWorkflow(WorkflowDto workflowDto) {

		List<String> errorMessages = new ArrayList<String>();

		Map<String, Procedure> mapProc = checkProcedures(workflowDto, errorMessages);
		Map<String, Rule> mapRule = checkRules(workflowDto, errorMessages);
		Map<String, Node> mapNodes = checkNodes(workflowDto, errorMessages);

		checkTransitionNodesExist(workflowDto, errorMessages, mapNodes);
		checkForDefaultTransition(workflowDto, errorMessages, mapNodes);

		long varCount = workflowDto.getVariables().stream().map(Variable::getName).distinct().count();
		if (varCount < workflowDto.getVariables().size()) {
			errorMessages.add(String.format("There are duplicated variable names"));
		}

		checkForNodeTypeDetails(workflowDto, errorMessages, mapProc, mapRule);
		
		checkThatProcExistsAsNodes(workflowDto, errorMessages, mapNodes);
		checkThatRuleExistsAsNodes(workflowDto, errorMessages, mapNodes);
		
		checkThatExistingProcessCurrentNodeStillExists(workflowDto, errorMessages, mapNodes);
		
		if (!errorMessages.isEmpty()) {
			throw new RuntimeException(String.format("Error loading workflow %s, there are %s validation errors: %s", 
				workflowDto.getCode(), errorMessages.size(), errorMessages.stream().collect(joining(", "))));
		}
	}

	private void checkThatExistingProcessCurrentNodeStillExists(WorkflowDto workflowDto, List<String> errorMessages,
		Map<String, Node> mapNodes)
	{
		List<String> processCurNodes = processDao.findProcessCurrentNodeByWorkflowCode(workflowDto.getCode());
		for (String curNode : processCurNodes) {
			if (!mapNodes.containsKey(curNode)) {
				errorMessages.add(String.format("Process current node %s not found", curNode));
			}
		}
	}

	private void checkThatRuleExistsAsNodes(WorkflowDto workflowDto, List<String> errorMessages,
		Map<String, Node> mapNodes)
	{
		for (Rule rule : workflowDto.getRules()) {
			if (!mapNodes.containsKey(rule.getNode_name())) {
				errorMessages.add(String.format("Node name not found for rule: %s", rule.getNode_name()));
			}
		}
	}

	private void checkThatProcExistsAsNodes(WorkflowDto workflowDto, List<String> errorMessages,
		Map<String, Node> mapNodes)
	{
		for (Procedure procedure : workflowDto.getProcedures()) {
			if (!mapNodes.containsKey(procedure.getNode_name())) {
				errorMessages.add(String.format("Node name not found for proc: %s", procedure.getNode_name()));
			}
		}
	}

	private void checkForNodeTypeDetails(WorkflowDto workflowDto, List<String> errorMessages,
		Map<String, Procedure> mapProc, Map<String, Rule> mapRule)
	{
		for (Node node : workflowDto.getNodes()) {
			if (node.getType() == NodeType.proc && !mapProc.containsKey(node.getName())) {
				errorMessages.add(String.format("Procedure not found for node: %s", node.getName()));
			} else if (node.getType() == NodeType.rule && !mapRule.containsKey(node.getName())) {
				errorMessages.add(String.format("Rule not found for node: %s", node.getName()));
			}
		}
	}

	// default for testing
	void checkForDefaultTransition(WorkflowDto workflowDto, List<String> errorMessages, Map<String, Node> mapNodes)
	{
		workflowDto.getTransitions().stream().forEach(t -> {
			Integer def = t.getIs_default();
			if (def == null || (def != 0 && def != 1))
				errorMessages.add(String.format("Transition from node %s, is_default must be either 0 or 1, got: %s", t.getNode_from(), def));
		});
		
		Map<String, List<Transition>> transitionMap = workflowDto.getTransitions().stream()
			.collect(Collectors.groupingBy(Transition::getNode_from));

		transitionMap.forEach((nodeName, tranList) -> {
			if (!mapNodes.containsKey(nodeName))
				return;

			NodeType nodeType = mapNodes.get(nodeName).getType();
			if (nodeType != NodeType.start && nodeType != NodeType.rule && nodeType != NodeType.proc)
				return;

			tranList.stream()
				.map(Transition::getScopes)
				.flatMap(s -> s.size() > 0 ? s.stream() : Stream.of((Scope) null))
				.distinct()
				.forEach(scope -> checkDefaultTransitionOnByScope(nodeName, tranList, scope, errorMessages));	
		});
	}

	private void checkDefaultTransitionOnByScope(String nodeName, List<Transition> tranList, Scope scope, List<String> errorMessages) {
		List<Transition> transactionsInScope;
		transactionsInScope = tranList.stream()
			.filter(t -> t.getScopes().size() == 0 || t.getScopes().contains(scope))	// transitions without any scopes are always welcome
			.collect(toList());

		if (transactionsInScope.size() == 0)
			return;
		
		if (transactionsInScope.size() > 1) {
			long howManyDefaults = transactionsInScope.stream().filter(ta -> ta.getIs_default() == 1).count();
			if (howManyDefaults == 0) {
				errorMessages.add(String.format("Transitions from node %s, have no default", nodeName));
			} else if (howManyDefaults > 1) {
				errorMessages.add(String.format("Transitions from node %s, have too many defaults", nodeName));
			}
		} else {
			Transition t = transactionsInScope.get(0);
			if (t.getIs_default() == 0 && t.getTran_rules().size() > 0)
				errorMessages.add(String.format("Transition from node %s, have transition rules set but no default", nodeName));
		}
	}

	private void checkTransitionNodesExist(WorkflowDto workflowDto, List<String> errorMessages, Map<String, Node> mapNodes)
	{
		for (Transition transition : workflowDto.getTransitions()) {
			if (!mapNodes.containsKey(transition.getNode_from())) {
				errorMessages.add(String.format("Transition node-from not found: %s", transition.getNode_from()));
			}
			if (!mapNodes.containsKey(transition.getNode_to())) {
				errorMessages.add(String.format("Transition node-to not found: %s", transition.getNode_to()));
			}
		}
	}

	private Map<String, Node> checkNodes(WorkflowDto workflowDto, List<String> errorMessages)
	{
		Map<String, Node> mapNodes = new HashMap<String, Node>();
		long nodeCount = workflowDto.getNodes().stream()
			.map(Node::getName)
			.distinct()
			.count();
		
		if (nodeCount < workflowDto.getNodes().size()) {
			errorMessages.add(String.format("There are duplicated nodes"));
		} else {
			mapNodes = workflowDto.getNodes().stream().collect(Collectors.toMap(Node::getName, node -> node));
		}
		
		return mapNodes;
	}

	private Map<String, Rule> checkRules(WorkflowDto workflowDto, List<String> errorMessages)
	{
		Map<String, Rule> mapRule = new HashMap<String, Rule>();
		long ruleCount = workflowDto.getRules().stream()
			.map(Rule::getNode_name)
			.distinct()
			.count();
		
		if (ruleCount < workflowDto.getRules().size()) {
			errorMessages.add(String.format("There are  duplicated rule nodes"));
		} else {
			mapRule = workflowDto.getRules().stream().collect(Collectors.toMap(Rule::getNode_name, rule -> rule));
		}
		
		return mapRule;
	}

	private Map<String, Procedure> checkProcedures(WorkflowDto workflowDto, List<String> errorMessages)
	{
		Map<String, Procedure> mapProc = new HashMap<String, Procedure>();
		long procCount = workflowDto.getProcedures().stream()
			.map(Procedure::getNode_name)
			.distinct()
			.count();
		
		if (procCount < workflowDto.getProcedures().size()) {
			errorMessages.add(String.format("There are duplicated proc nodes"));
		} else {
			mapProc = workflowDto.getProcedures().stream().collect(Collectors.toMap(Procedure::getNode_name, proc -> proc));
		}
		
		return mapProc;
	}

	private void jsonSchemaValidate(String jsonString, String jarfile) throws Exception {
		JsonSchemaFactory factory = JsonSchemaFactory.getInstance(VersionFlag.V7);
		JsonSchema schema;
		try (InputStream isSchema = getClass().getClassLoader().getResourceAsStream("abaco-workflow-schema.json")) {
			schema = factory.getSchema(isSchema);
		}
		JsonNode jsonRoot = objectMapper.readTree(jsonString);
		Set<ValidationMessage> errors = schema.validate(jsonRoot);
		if (!errors.isEmpty()) {
			for (ValidationMessage validationMessage : errors) {
				log.error("Error validating schema on abaco-workflow.json {} in workflow jar file: {}", validationMessage.getMessage(), jarfile);
			}
			throw new RuntimeException(String.format("Errors validating json schema in workflow jar :  %s ", jarfile));
		}
	}
	
	private URL copyDynamicWfInTmpForClassloader(File wfJar, File dynamicWfPath) throws IOException {
		File tmpDirectoryPath = new File(dynamicWfPath, DYNAMIC_WF_TEMPORARY_DIR_SUFFIX + "/" + System.currentTimeMillis() + "-" + wfJar.getName());
		Files.copy(wfJar.toPath(), tmpDirectoryPath.toPath(), StandardCopyOption.REPLACE_EXISTING);
		return tmpDirectoryPath.toURI().toURL();
	}

	// for testing
	void clearClassLoaders() throws IOException
	{
		for (WorkflowClassLoader cl : classLoaderMap.values())
			cl.close();
		
		classLoaderMap.clear();
	}
}
