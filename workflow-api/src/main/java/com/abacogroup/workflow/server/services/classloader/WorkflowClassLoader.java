package com.abacogroup.workflow.server.services.classloader;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static java.util.stream.Collectors.joining;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.IOUtils;

import com.abacogroup.workflow.sdk.proc.Procedure;

import groovy.lang.GroovyClassLoader;

public class WorkflowClassLoader extends URLClassLoader
{

	private GroovyClassLoader groovyClassLoader;
	private ConcurrentHashMap<String, Class<Procedure>> loadedProcedures;
	private boolean dynamic;
	
	public WorkflowClassLoader(URL[] urls, boolean dynamic, ClassLoader parent)
	{
		super(urls, parent);
		this.dynamic = dynamic;
		this.groovyClassLoader = new GroovyClassLoader(this);
		this.loadedProcedures = new ConcurrentHashMap<>();
	}

	@Override
	public void close() throws IOException
	{
		groovyClassLoader.close();
		super.close();
	}
	
	public Class<Procedure> getGroovyProcedureClass(String scriptName)
	{
		Class<Procedure> clazz = loadedProcedures.get(scriptName);
		if (clazz != null)
			return clazz;
	
		return loadGroovyProcedureClass(scriptName);
	}

	@SuppressWarnings("unchecked")
	private synchronized Class<Procedure> loadGroovyProcedureClass(String scriptName)
	{
		InputStream in = getResourceAsStream(scriptName);
		if (in == null)
			throw new IllegalStateException(scriptName + " not found");
			
		try (InputStreamReader reader = new InputStreamReader(in, StandardCharsets.UTF_8)) {
			String src = IOUtils.readLines(reader)
				.stream()
				.collect(joining("\n"));
			
			src = "import com.abacogroup.workflow.sdk.proc.*;\n"
				+ "import com.abacogroup.workflow.sdk.beans.*;\n"
				+ "\n"
				+ src;
			
			Class<?> clazz = groovyClassLoader.parseClass(src, scriptName);
			if (!Procedure.class.isAssignableFrom(clazz))
				throw new UnsupportedOperationException(scriptName + " is not a Procedure");
			
			loadedProcedures.put(scriptName, (Class<Procedure>) clazz);
			
			return (Class<Procedure>) clazz;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean isDynamic()
	{
		return dynamic;
	}

}
