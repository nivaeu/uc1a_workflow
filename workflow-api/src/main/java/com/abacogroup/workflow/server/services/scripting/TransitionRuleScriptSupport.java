package com.abacogroup.workflow.server.services.scripting;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import org.codehaus.groovy.ast.stmt.ForStatement;
import org.codehaus.groovy.ast.stmt.WhileStatement;
import org.codehaus.groovy.control.CompilationFailedException;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.codehaus.groovy.control.customizers.ImportCustomizer;
import org.codehaus.groovy.control.customizers.SecureASTCustomizer;
import org.codehaus.groovy.syntax.Types;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.lang.Script;

public class TransitionRuleScriptSupport
{

	public static Script createScript(Binding binding, String formula) throws CompilationFailedException
	{
		GroovyShell shell = new GroovyShell(binding, createCompilerConfiguration());
		return shell.parse(formula);
	}
	
	private static CompilerConfiguration createCompilerConfiguration()
	{
		CompilerConfiguration cfg = new CompilerConfiguration();
		addSecurityConfiguration(cfg);
		return cfg;
	}

	private static void addSecurityConfiguration(CompilerConfiguration cfg)
	{
		final ImportCustomizer imports = new ImportCustomizer()
			.addStaticStars("java.lang.Math")
			.addStarImports("com.abacogroup.workflow.sdk.proc", "com.abacogroup.workflow.sdk.beans");
		
		SecureASTCustomizer customizer = new SecureASTCustomizer();
		customizer.setAllowedImports(new ArrayList<>(0));
		customizer.setAllowedStarImports(Arrays.asList("com.abacogroup.workflow.sdk.proc", "com.abacogroup.workflow.sdk.beans"));
		customizer.setAllowedReceiversClasses(Arrays.asList(
			Math.class,
			Boolean.class,
            Integer.class,
            Float.class,
            Double.class,
            Long.class,
            Map.class,
            Object.class,
            BigDecimal.class));
		customizer.setAllowedStaticImports(new ArrayList<>(0));
		customizer.setAllowedStaticStarImports(Arrays.asList("java.lang.Math"));
		customizer.setAllowedTokens(Arrays.asList(
			Types.PLUS,
			Types.MINUS,
			Types.MULTIPLY,
			Types.DIVIDE,
			Types.MOD,
			Types.POWER,
			Types.PLUS_PLUS,
			Types.MINUS_MINUS,
			Types.COMPARE_EQUAL,
			Types.COMPARE_NOT_EQUAL,
			Types.COMPARE_LESS_THAN,
			Types.COMPARE_LESS_THAN_EQUAL,
			Types.COMPARE_GREATER_THAN,
			Types.COMPARE_GREATER_THAN_EQUAL,
			Types.DIVIDE,
			Types.DIVIDE_EQUAL,
			Types.EQUAL,
			Types.LEFT_PARENTHESIS,
			Types.LEFT_SQUARE_BRACKET,
			Types.MINUS,
			Types.MINUS_EQUAL,
			Types.MINUS_MINUS,
			Types.MULTIPLY,
			Types.MULTIPLY_EQUAL,
			Types.POWER,
			Types.PLUS,
			Types.PLUS_EQUAL,
			Types.PLUS_PLUS,
			Types.RIGHT_PARENTHESIS,
			Types.RIGHT_SQUARE_BRACKET));
		customizer.setClosuresAllowed(false);
		customizer.setConstantTypesClassesWhiteList(Arrays.asList(
			Object.class,
			Boolean.class,
			Integer.class,
            Float.class,
            Long.class,
            Double.class,
            BigDecimal.class,
            String.class,
            Boolean.TYPE,
            Integer.TYPE,
            Long.TYPE,
            Float.TYPE,
            Double.TYPE));
		customizer.setDisallowedStatements(Arrays.asList(
			ForStatement.class, 
			WhileStatement.class));
		customizer.setMethodDefinitionAllowed(false);
		
		cfg.addCompilationCustomizers(imports, customizer);
		// cfg.setDebug(true);
	}

}
