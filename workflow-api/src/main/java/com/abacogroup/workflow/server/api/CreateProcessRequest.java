package com.abacogroup.workflow.server.api;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

@Data
public class CreateProcessRequest
{
	private String workflow_code;
	private String scope;
	private boolean return_global_vars;
	private Map<String, ? super Object> global_vars = new HashMap<>();
	
	public CreateProcessRequest()
	{
	}

	public CreateProcessRequest(String workflow_code, String scope)
	{
		this(workflow_code, scope, false);
	}

	public CreateProcessRequest(String workflow_code, String scope, boolean return_global_vars)
	{
		this.workflow_code = workflow_code;
		this.scope = scope;
		this.return_global_vars = return_global_vars;
	}
	
}
