package com.abacogroup.workflow.server.services;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.abacogroup.workflow.server.api.WorkflowDto;
import com.abacogroup.workflow.server.api.WorkflowDto.Node;
import com.abacogroup.workflow.server.db.ProcessDao;
import com.abacogroup.workflow.server.db.WorkflowDao;
import com.fasterxml.jackson.databind.ObjectMapper;

public class WorkflowServiceTest
{
	
	private ObjectMapper mapper = new ObjectMapper();
	private ProcessDao processDao;
	private WorkflowDao workflowDao;
	private WorkflowService service;

	private File tempDir;
	
	private boolean dirInitialized;
	
	@BeforeEach
	public void initDynamicDir() {
		processDao = mock(ProcessDao.class);
		workflowDao = mock(WorkflowDao.class);
		service = new WorkflowService(processDao, workflowDao, new ArrayList<>(0), mapper);

		if (dirInitialized)
			return;

		tempDir = new File(System.getProperty("java.io.tmpdir"));
		new File(tempDir, WorkflowService.DYNAMIC_WF_TEMPORARY_DIR_SUFFIX).mkdir();
		
		dirInitialized = true;
	}
	
	@AfterEach
	public void shutdown() throws IOException {
		service.clearClassLoaders();
	}

	@Test
	public void whenLoadingWorkflows_thenCheckDynamicness() throws Exception {
		File file = new File(getClass().getClassLoader().getResource("workflow/GroovyWorkflow_A.jar").toURI());
		service.loadWorkflow(file);
		
		assertThrows(UnsupportedOperationException.class, () -> service.loadWorkflow(file, tempDir));
	}

	
	@Test
	public void whenNoDefaultsOnNodeWithTwoTransitions_thanError() throws IOException {
		WorkflowDto wf = readResourceAsString("transition-default-test/no-default.json");
		ArrayList<String> msg = new ArrayList<>();
		service.checkForDefaultTransition(wf, msg, nodes(wf));
		assertEquals(1, msg.size());
		assertTrue(msg.get(0).endsWith("no default"));
	}
	
	@Test
	public void whenNoDefaultsOnNodeWithOneTransitionAndRule_thanError() throws IOException {
		WorkflowDto wf = readResourceAsString("transition-default-test/no-default-single-transition.json");
		ArrayList<String> msg = new ArrayList<>();
		service.checkForDefaultTransition(wf, msg, nodes(wf));
		assertEquals(1, msg.size());
		assertTrue(msg.get(0).endsWith("have transition rules set but no default"));
	}
	
	@Test
	public void whenTwoDefaultTransitions_thanError() throws IOException {
		WorkflowDto wf = readResourceAsString("transition-default-test/two-defaults.json");
		ArrayList<String> msg = new ArrayList<>();
		service.checkForDefaultTransition(wf, msg, nodes(wf));
		assertEquals(1, msg.size());
		assertTrue(msg.get(0).endsWith("too many defaults"));
	}
	
	@Test
	public void whenTwoDefaultTransitionsOnSameScope_thanError() throws IOException {
		WorkflowDto wf = readResourceAsString("transition-default-test/two-defaults-same-scope.json");
		ArrayList<String> msg = new ArrayList<>();
		service.checkForDefaultTransition(wf, msg, nodes(wf));
		assertEquals(1, msg.size());
		assertTrue(msg.get(0).endsWith("too many defaults"));
	}

	@Test
	public void whenTwoDefaultTransitionsInDifferentScopes_thancontinue() throws IOException {
		WorkflowDto wf = readResourceAsString("transition-default-test/two-defaults-different-scopes.json");
		ArrayList<String> msg = new ArrayList<>();
		service.checkForDefaultTransition(wf, msg, nodes(wf));
		assertEquals(0, msg.size());
	}

	private Map<String, Node> nodes(WorkflowDto wf)
	{
		Map<String, Node> ret = new HashMap<>();
		wf.getNodes().forEach(n -> ret.put(n.getName(), n));
		return ret;
	}

	private WorkflowDto readResourceAsString(String resource) throws IOException
	{
		try (InputStream in = getClass().getClassLoader().getResourceAsStream(resource)) {
			String content =  IOUtils.toString(in, StandardCharsets.UTF_8);
			return mapper.readValue(content, WorkflowDto.class);
		}
	}
	
}
