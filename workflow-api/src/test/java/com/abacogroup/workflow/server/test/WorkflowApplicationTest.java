package com.abacogroup.workflow.server.test;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import com.abacogroup.workflow.server.WorkflowApplication;
import com.abacogroup.workflow.server.WorkflowConfiguration;

import io.dropwizard.setup.Environment;

public class WorkflowApplicationTest extends WorkflowApplication {

	@Override
	public void run(WorkflowConfiguration config, Environment environment) throws Exception {		
	}


}
