package com.abacogroup.workflow.server.test.model;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.sql.Timestamp;
import java.util.List;

import com.abacogroup.workflow.sdk.beans.WorkflowMessage;

import lombok.Data;

@Data
public class TstTransitionsHistoryResponse
{
	
	private List<TstTransitionDetails> transitions;
	
	@Data
	public static class TstTransitionDetails {
		private Long id;
		private Timestamp end_date;
		private Timestamp start_date;
		private String user_id;
		private Long wor_process_id;
		private Integer wor_workflow_transition_id;
		private boolean is_failed;
		private String description;
		private String node_from;
		private String node_to;
		private List<WorkflowMessage> messages;
	}
}
