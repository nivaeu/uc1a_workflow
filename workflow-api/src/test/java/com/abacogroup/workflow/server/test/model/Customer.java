package com.abacogroup.workflow.server.test.model;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

public class Customer {

	private CustomerType type;

	public int years;

	private int discount;

	private Nested nested;

	private ObjVar objvar;

	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public Customer(CustomerType type, int years, int discount, Nested nested, ObjVar objvar) {
		super();
		this.type = type;
		this.years = years;
		this.discount = discount;
		this.nested = nested;
		this.objvar = objvar;
	}


	public Customer(CustomerType type, int numOfYears) {
		super();
		this.type = type;
		this.years = numOfYears;
	}

	public CustomerType getType() {
		return type;
	}

	public void setType(CustomerType type) {
		this.type = type;
	}

	public int getYears() {
		return years;
	}

	public void setYears(int years) {
		this.years = years;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public enum CustomerType {
		INDIVIDUAL, BUSINESS, TEST;
	}

	public Nested getNested() {
		return nested;
	}

	public void setNested(Nested nested) {
		this.nested = nested;
	}

	public ObjVar getObjvar() {
		return objvar;
	}

	public void setObjvar(ObjVar objvar) {
		this.objvar = objvar;
	}

	@Override
	public String toString() {
		return "Customer [type=" + type + ", years=" + years + ", discount=" + discount + ", nested=" + nested + ", objvar=" + objvar + "]";
	}

}
