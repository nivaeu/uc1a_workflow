package com.abacogroup.workflow.server.test;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.IOUtils;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;

import com.abacogroup.workflow.sdk.beans.WorkflowMessage;
import com.abacogroup.workflow.server.WorkflowApplication;
import com.abacogroup.workflow.server.WorkflowConfiguration;
import com.abacogroup.workflow.server.api.CreateProcessRequest;
import com.abacogroup.workflow.server.api.TransitionRequest;
import com.abacogroup.workflow.server.api.TransitionsResponse;
import com.abacogroup.workflow.server.api.WorkflowDto;
import com.abacogroup.workflow.server.db.ProcessDao;
import com.abacogroup.workflow.server.db.WorkflowDao;
import com.abacogroup.workflow.server.db.ntt.WorkflowTransitionWithScope;
import com.abacogroup.workflow.server.services.WorkflowService;
import com.abacogroup.workflow.server.test.model.Customer;
import com.abacogroup.workflow.server.test.model.Customer.CustomerType;
import com.abacogroup.workflow.server.test.model.ObjVar;
import com.abacogroup.workflow.server.test.model.TstProcessResponse;
import com.abacogroup.workflow.server.test.model.TstTransitionsHistoryResponse;
import com.abacogroup.workflow.server.test.model.TstTransitionsHistoryResponse.TstTransitionDetails;

import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.testing.DropwizardTestSupport;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit5.DropwizardAppExtension;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.util.Duration;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;

@ExtendWith(DropwizardExtensionsSupport.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class WorkflowIntegrationTest {

	private static final String CONFIG_PATH_TEST = ResourceHelpers.resourceFilePath("test_config.yml");
	private static final String CONFIG_PATH_TEST_DB = ResourceHelpers.resourceFilePath("test_config_db.yml");
	private static final String MIGRATE_PATH_TEST = "test_migrations.xml";
	private static Long processId = null;
	private static Client client = null;
	private final static JerseyClientConfiguration jerseyClientConfiguration = new JerseyClientConfiguration();

	private static DataSourceFactory dsf;
	private static String baseUrl;

	private static final DropwizardAppExtension<WorkflowConfiguration> APP_EXT_DB = new DropwizardAppExtension<>(WorkflowApplicationTest.class, CONFIG_PATH_TEST_DB);

	public static final DropwizardTestSupport<WorkflowConfiguration> APP_TEST = new DropwizardTestSupport<WorkflowConfiguration>(WorkflowApplication.class, CONFIG_PATH_TEST);

	@BeforeAll
	private static void migrate() throws Exception {
		// CREATE H2 IN MEMORY DB
		DataSourceFactory dataSourceFactory = APP_EXT_DB.getConfiguration().getDataSourceFactory();
		Properties info = new Properties();
		info.setProperty("user", dataSourceFactory.getUser());
		info.setProperty("password", dataSourceFactory.getPassword());
		org.h2.jdbc.JdbcConnection h2Conn = new org.h2.jdbc.JdbcConnection(dataSourceFactory.getUrl(), info);
		JdbcConnection conn = new JdbcConnection(h2Conn);
		Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(conn);
		Liquibase liquibase = new Liquibase(MIGRATE_PATH_TEST, new ClassLoaderResourceAccessor(), database);
		String ctx = null;
		liquibase.update(ctx);
		APP_TEST.before(); // START APP
		jerseyClientConfiguration.setTimeout(Duration.minutes(1));
		baseUrl = "http://localhost:" + APP_TEST.getLocalPort() + "/workflow/api/v1/processes/";
		dsf = APP_TEST.getConfiguration().getDataSourceFactory();
		client = new JerseyClientBuilder(APP_TEST.getEnvironment()).using(jerseyClientConfiguration).build("test client");
		liquibase.close();
	}

	@AfterAll
	public static void afterClass() {
		APP_TEST.after(); // STOP APP
	}
	
	@Test
	public void whenLibsArePresent_thenMustBeUsableFroMWorflows() throws IOException {
		CreateProcessRequest cpr = new CreateProcessRequest("NeedsLib", "scope");

		Response response = client.target(baseUrl).request().post(Entity.entity(cpr, MediaType.APPLICATION_JSON_TYPE));
		TstProcessResponse resBody = response.readEntity(TstProcessResponse.class);

		assertEquals(response.getStatus(), Status.OK.getStatusCode());
		assertEquals(1, resBody.getMessages().size());
		assertEquals("common", resBody.getMessages().get(0).getCode());
	}

	@Test
	public void whenNodeHasMetadata_thenReturnThem() {
		CreateProcessRequest cpr = new CreateProcessRequest();
		cpr.setScope("string");
		cpr.setWorkflow_code("TestMetadata");
		Map<String, Object> globMap = new HashMap<String, Object>();
		cpr.setGlobal_vars(globMap);

		Response response = client.target(baseUrl).request().post(Entity.entity(cpr, MediaType.APPLICATION_JSON_TYPE));
		TstProcessResponse resBody = response.readEntity(TstProcessResponse.class);

		assertEquals(response.getStatus(), Status.OK.getStatusCode());
		
		Map<String, Object> meta = resBody.getCurrent_node_metadata();
		assertNotNull(meta);
		assertEquals("ABACO", meta.get("owner"));
		assertEquals("abacogroup.eu", meta.get("domain"));
	}
	
	@Test
	public void whenCreatingProcess_thenPersistOnlyCorrectVariables()
	{
		CreateProcessRequest cpr = new CreateProcessRequest();
		cpr.setScope("scope");
		cpr.setWorkflow_code("TestVariablePersist");
		Map<String, Object> vars = new HashMap<String, Object>();
		vars.put("persisted", "persisted");
		vars.put("notPersisted", "notPersisted");
		cpr.setGlobal_vars(vars);

		Response response = client.target(baseUrl).request().post(Entity.entity(cpr, MediaType.APPLICATION_JSON_TYPE));
		assertEquals(Status.OK.getStatusCode(), response.getStatus());

		TstProcessResponse resBody = response.readEntity(TstProcessResponse.class);
		assertEquals("status", resBody.getCurrent_node_name());

		TransitionsResponse resp = client.target(baseUrl)
			.path("" + resBody.getProcess_id())
			.path("/transitions")
			.queryParam("scope", "scope")
			.request()
			.get(TransitionsResponse.class);
		
		assertNotEquals(0, resp.getAllowed_transitions().size());
		
		WorkflowTransitionWithScope trans = resp.getAllowed_transitions().get(0);
		
		response = client.target(baseUrl)
			.path("" + resBody.getProcess_id())
			.path("/transitions")
			.path("" + trans.getId())
			.request().put(Entity.entity(new TransitionRequest("scope", false), MediaType.APPLICATION_JSON_TYPE));
		
		resBody = response.readEntity(TstProcessResponse.class);
		assertEquals(Status.OK.getStatusCode(), response.getStatus());
		assertEquals("end", resBody.getCurrent_node_name());		
	}
	
	@Test
	void whenValidateInvalidWorkflow_thenThrowExceptionWitherrors() throws Exception {
		
		Exception exception = assertThrows(RuntimeException.class, () -> {
			InputStream jsonStream = getClass().getClassLoader().getResourceAsStream("abaco-workflow-validation-test.json");
			final Jdbi jdbi = new JdbiFactory().build(APP_TEST.getEnvironment(), dsf, "dbi_wf_dao");
			ProcessDao processDao = jdbi.onDemand(ProcessDao.class);
			WorkflowDao workflowDao = jdbi.onDemand(WorkflowDao.class);
			String wfJson = IOUtils.toString(jsonStream, StandardCharsets.UTF_8);
			jsonStream.close();
			WorkflowDto workflowDto = APP_TEST.getObjectMapper().readValue(wfJson, WorkflowDto.class);
			WorkflowService workflowService = new WorkflowService(processDao, workflowDao, new ArrayList<>(0), APP_TEST.getEnvironment().getObjectMapper());
			workflowService.validateWorkflow(workflowDto);
		});
		
		String expectedMessage = "there are 11 validation errors";
		String errMessage = exception.getMessage();
		assertTrue(errMessage.contains(expectedMessage));
	}

	@Test
	@Order(-2)
	void whenStartApplication_thenCreateWorkflowSuccesfully() throws Exception {
		// should create workflow from WFTEST_B-001.jar
		final Jdbi jdbi = new JdbiFactory().build(APP_TEST.getEnvironment(), dsf, "dbi_wf");
		try (Handle h = jdbi.open()) {
			Optional<Integer> wfId = h.createQuery("SELECT id FROM wor_workflow WHERE code=:code").bind("code", "WFTEST_B").mapTo(Integer.class).findOne();
			assertTrue(wfId.isPresent());
			Optional<Integer> nodeId = h.createQuery("SELECT id FROM wor_workflow_node WHERE wor_workflow_id=:workflow_id AND name=:node_name").bind("workflow_id", wfId)
				.bind("node_name", "nodeS").mapTo(Integer.class).findOne();
			assertTrue(nodeId.isPresent());
			Optional<Integer> transitionId = h.createQuery("SELECT id FROM wor_workflow_transition WHERE wor_workflow_id=:workflow_id AND node_from=:node_id")
				.bind("workflow_id", wfId).bind("node_id", nodeId).mapTo(Integer.class).findOne();
			assertTrue(transitionId.isPresent());
		}

	}

	@Test
	@Order(-1)
	void whenCreateProcessForNewWorkflowAddedOnStartup_thenReturnsHttp200() throws Exception {
		CreateProcessRequest cpr = new CreateProcessRequest();
		cpr.setScope("string");
		cpr.setWorkflow_code("WFTEST_B"); // ADDED ON STARTUP
		Map<String, Object> globMap = new HashMap<String, Object>();
		globMap.put("string", "TestString");
		cpr.setGlobal_vars(globMap);

		Response response = client.target(baseUrl).request().post(Entity.entity(cpr, MediaType.APPLICATION_JSON_TYPE));

		assertEquals(response.getStatus(), Status.OK.getStatusCode());

	}

	@Test
	@Order(0)
	void whenCreateProcessWithWrongWorkflowId_thenReturnsNotFound() throws Exception {
		CreateProcessRequest cpr = new CreateProcessRequest();
		cpr.setScope("scope1");
		cpr.setWorkflow_code("222222");
		// cpr.setWorkflow_id(2222); // wrong id
		Map<String, Object> globMap = new HashMap<String, Object>();
		ObjVar objVar = new ObjVar("testObjVar", 0);
		globMap.put("objvar1", objVar);
		globMap.put("var2", "val2");
		globMap.put("var3", 3);
		globMap.put("timeloc", "2012-04-23T18:25:43.511Z");
		globMap.put("customer", new Customer(CustomerType.INDIVIDUAL, 2, 0, null, objVar)); // REQUIRED INPUT VAR
		cpr.setGlobal_vars(globMap);

		Response response = client.target(baseUrl).request().post(Entity.entity(cpr, MediaType.APPLICATION_JSON_TYPE));
		TstProcessResponse resBody = response.readEntity(TstProcessResponse.class);
		// System.out.println(APP_TEST.getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(resBody));

		assertEquals(resBody.getProcess_id(), null);
		assertEquals(response.getStatus(), Status.NOT_FOUND.getStatusCode());
	}

	//FOLLOWING TESTS ARE ALL BASED ON WORKFLOW 'WFTEST_A' DATA LOADED FROM DB BY 002_data.xml LIQUIBASE,  
	//NO UPDATES ARE LOADED FROM WFTEST_A-001.JAR ON STARTUP
	@Test
	@Order(1)
	void whenCreateProcessWithoutRequiredInputVar_thenReturnsBadRequest() throws Exception {
		CreateProcessRequest cpr = new CreateProcessRequest();
		cpr.setScope("scope1");
		cpr.setWorkflow_code("WFTEST_A");	
		Map<String, Object> globMap = new HashMap<String, Object>();
		ObjVar objVar = new ObjVar("testObjVar", 0);
		globMap.put("objvar1", objVar);
		globMap.put("var2", "val2");
		globMap.put("var3", 3);
		globMap.put("timeloc", "2012-04-23T18:25:43.511Z");
		// globMap.put("customer", new Customer(CustomerType.INDIVIDUAL, 2, 0, null,
		// objVar)); // REQUIRED INPUT VAR
		cpr.setGlobal_vars(globMap);

		Response response = client.target(baseUrl).request().post(Entity.entity(cpr, MediaType.APPLICATION_JSON_TYPE));
		TstProcessResponse resBody = response.readEntity(TstProcessResponse.class);
		// System.out.println(APP_TEST.getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(resBody));

		assertEquals(resBody.getProcess_id(), null);
		assertEquals(response.getStatus(), Status.BAD_REQUEST.getStatusCode());
	}

	@Test
	@Order(2)
	void whenCreateProcessWithInputVarNotDefinedInWorkflow_thenReturnsBadRequest() throws Exception {
		CreateProcessRequest cpr = new CreateProcessRequest();
		cpr.setScope("scope1");
		cpr.setWorkflow_code("WFTEST_A");
		Map<String, Object> globMap = new HashMap<String, Object>();
		ObjVar objVar = new ObjVar("testObjVar", 0);
		globMap.put("objvar1", objVar);
		globMap.put("fakevar", 0); // INPUT VAR NOT DEFINED
		globMap.put("var2", "val2");
		globMap.put("var3", 3);
		globMap.put("timeloc", "2012-04-23T18:25:43.511Z");
		globMap.put("customer", new Customer(CustomerType.INDIVIDUAL, 2, 0, null, objVar)); // REQUIRED INPUT VAR
		cpr.setGlobal_vars(globMap);

		Response response = client.target(baseUrl).request().post(Entity.entity(cpr, MediaType.APPLICATION_JSON_TYPE));
		TstProcessResponse resBody = response.readEntity(TstProcessResponse.class);
		// System.out.println(APP_TEST.getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(resBody));

		assertEquals(resBody.getProcess_id(), null);
		assertEquals(response.getStatus(), Status.BAD_REQUEST.getStatusCode());
	}

	@Test
	@Order(3)
	void whenCreateProcess_thenReturnsSuccesfullyAndTheLastTransitionLog() throws Exception {
		CreateProcessRequest cpr = new CreateProcessRequest("WFTEST_A", "scope1");
		Map<String, Object> globMap = cpr.getGlobal_vars();
		ObjVar objVar = new ObjVar("testObjVar", 0);
		globMap.put("objvar1", objVar);
		globMap.put("var2", "val2");
		globMap.put("var3", 3);
		globMap.put("timeloc", "2012-04-23T18:25:43.511Z");
		globMap.put("customer", new Customer(CustomerType.INDIVIDUAL, 2, 0, null, objVar));// WITH AGE 2 TRANSITION RULES WILL FORWARD TO NODE 13

		TstProcessResponse response = client.target(baseUrl).request().post(Entity.entity(cpr, MediaType.APPLICATION_JSON_TYPE)).readEntity(TstProcessResponse.class);
		// System.out.println(APP_TEST.getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(response));
		processId = response.getProcess_id();

		assertNotNull(response.getProcess_id());
		assertEquals(false, response.getTransition_log().isFailed());
	}

	@Test
	@Order(4)
	void whenGetAvailableTransitions_thenReturnsAllOnesInScopeOrWithScopeNull() throws Exception {
		// PASS A CORRECT SCOPE
		final String url = baseUrl + processId + "/transitions?scope=scope1";

		TransitionsResponse response = client.target(url).request().get(TransitionsResponse.class);
		// System.out.println(APP_TEST.getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(response));

		assertEquals(response.getAllowed_transitions().size(), 2);
		assertEquals(response.getAllowed_transitions().get(1).getId(), 8);
		assertEquals(response.getAllowed_transitions().get(0).getNode_to(), 12);
		assertEquals(response.getAllowed_transitions().get(0).getScope(), "scope1");
		assertEquals(response.getAllowed_transitions().get(1).getNode_to(), 14);

	}

	@Test
	@Order(5)
	void whenGetAvailableTransitionsNotInScope_thenReturnsOnlyOnesDefinedWithNullScope() throws Exception {

		// JUST PASS A WRONG SCOPE
		final String url = baseUrl + processId + "/transitions?scope=scope99999";

		TransitionsResponse response = client.target(url).request().get(TransitionsResponse.class);
		// System.out.println(APP_TEST.getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(response));

		assertEquals(response.getAllowed_transitions().size(), 1);
		assertEquals(response.getAllowed_transitions().get(0).getScope(), null);

	}

	@Test
	@Order(6)
	void whenUserStartTransitionFromStatus_thenProcessContinueSuccesfully() throws Exception {

		final String url = baseUrl + processId + "/transitions/8";

		TstProcessResponse response = client.target(url).request().put(Entity.entity(new TransitionRequest("scope1", false), MediaType.APPLICATION_JSON_TYPE)).readEntity(TstProcessResponse.class);
		// System.out.println(APP_TEST.getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(response));

		assertEquals(false, response.getTransition_log().isFailed());
	}

	@Test
	void whenNodeExecutionThrowsException_thenTransitionFailsAndSetStatusNodeToPreviuosAndLogTransitionAsFailed() throws Exception {
		CreateProcessRequest request = new CreateProcessRequest("WFTEST_A", "scope1");
		Map<String, Object> globalVars = request.getGlobal_vars();
		ObjVar objVar = new ObjVar("testObjVar", 0);
		globalVars.put("objvar1", objVar);
		globalVars.put("var2", "val2");
		globalVars.put("var3", 3);
		globalVars.put("timeloc", "2012-04-23T18:25:43.511Z");
		globalVars.put("customer", new Customer(CustomerType.INDIVIDUAL, 22, 0, null, objVar)); // WITH AGE 22 TRANSITION RULES WILL FORWARD TO NODE 8

		// SHOULD FAILS DUE PROC NODE METHOD THROWS EXCEPTION
		Response response = client.target(baseUrl).request().post(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE));
		assertEquals(500, response.getStatus());
		
		TstProcessResponse entity = response.readEntity(TstProcessResponse.class);
		// System.out.println(APP_TEST.getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(response));

		assertEquals(true, entity.getTransition_log().isFailed());
		assertEquals(entity.getCurrent_node_name(), "name3"); // setting the current node to previous status
	}

	@Test
	@Order(8)
	void whenWorkflowIsUpdated_thenCurrentProcessChanges() throws Exception {
		CreateProcessRequest cpr = new CreateProcessRequest();
		cpr.setScope("scope1");
		// cpr.setWorkflow_id(1);
		cpr.setWorkflow_code("WFTEST_A");
		Map<String, Object> globMap = new HashMap<String, Object>();
		ObjVar objVar = new ObjVar("testObjVar", 0);
		globMap.put("objvar1", objVar);
		globMap.put("var2", "val2");
		globMap.put("var3", 3);
		globMap.put("timeloc", "2012-04-23T18:25:43.511Z");
		globMap.put("customer", new Customer(CustomerType.INDIVIDUAL, 2, 0, null, objVar));// WITH AGE 2 TRANSITION RULES WILL FORWARD TO NODE 13
		cpr.setGlobal_vars(globMap);

		TstProcessResponse procResponse = client.target(baseUrl).request().post(Entity.entity(cpr, MediaType.APPLICATION_JSON_TYPE)).readEntity(TstProcessResponse.class);
		// System.out.println(APP_TEST.getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(procResponse));
		processId = procResponse.getProcess_id();

		final String urlGet = baseUrl + processId + "/transitions?scope=scope1";

		assertEquals(procResponse.getCurrent_node_name(), "name10");
		// RETURN TWO TRANSITION 5 & 8 AVAILABLE FROM CURRENT NODE STATUS 'name10'
		TransitionsResponse tranResponse = client.target(urlGet).request().get(TransitionsResponse.class);
		// System.out.println(APP_TEST.getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(tranResponse));
		assertEquals(tranResponse.getAllowed_transitions().size(), 2);
		assertEquals(tranResponse.getAllowed_transitions().get(0).getId(), 5);
		assertEquals(tranResponse.getAllowed_transitions().get(1).getId(), 8);

		final Jdbi jdbi = new JdbiFactory().build(APP_TEST.getEnvironment(), dsf, "dbi");
		// WORKFLOW UPDATE SIMULATION CHANGING A NODE DATE VALIDITY
		try (Handle h = jdbi.open()) {
			h.execute("UPDATE wor_workflow_node SET dt_delete = TO_DATE('2018/01/30', 'YYYY/MM/DD') WHERE id=10");
			h.execute("UPDATE wor_workflow_node SET dt_delete = TO_DATE('2999/01/30', 'YYYY/MM/DD') WHERE id=20");
			h.close();
		}

		// RETURN NEW TRANSITIONS FOR CURRENT PROCESS
		tranResponse = client.target(urlGet).request().get(TransitionsResponse.class);
		// System.out.println(APP_TEST.getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(tranResponse));
		assertEquals(tranResponse.getAllowed_transitions().size(), 1);
		assertEquals(tranResponse.getAllowed_transitions().get(0).getId(), 13);
	}
	
	@Test
	void whenGetTransitionsHistory_thenGetSuccesfulResponse() throws Exception {
		CreateProcessRequest cpr = new CreateProcessRequest("GroovyWorkflow_A", "scope1");
		Response response = client.target(baseUrl).request().post(Entity.entity(cpr, MediaType.APPLICATION_JSON_TYPE));
		TstProcessResponse resBody = response.readEntity(TstProcessResponse.class);

		TstTransitionsHistoryResponse responseTransitionsHistory = client.target(baseUrl)
			.path(resBody.getProcess_id() + "")
			.path("/transitions-history")
			.request().get(TstTransitionsHistoryResponse.class);
		
		List<TstTransitionDetails> transitions = responseTransitionsHistory.getTransitions();
		assertNotNull(transitions);
		assertNotEquals(0, transitions.size());
	}
	
	@Test
	void whenGetTransitionsHistory_thenGetMessagesBack() throws Exception {
		CreateProcessRequest cpr = new CreateProcessRequest("GroovyWorkflow_A", "scope1");
		Response response = client.target(baseUrl).request().post(Entity.entity(cpr, MediaType.APPLICATION_JSON_TYPE));
		TstProcessResponse resBody = response.readEntity(TstProcessResponse.class);
		List<WorkflowMessage> messages = resBody.getMessages();

		TstTransitionsHistoryResponse responseTransitionsHistory = client.target(baseUrl)
			.path(resBody.getProcess_id() + "")
			.path("/transitions-history")
			.request().get(TstTransitionsHistoryResponse.class);
		
		List<TstTransitionDetails> transitions = responseTransitionsHistory.getTransitions();
		TstTransitionDetails last = transitions.get(0);
		
		assertNotNull(last.getMessages());
		assertEquals(messages, last.getMessages());
		
	}
	
	@Test
	public void whenGroovyProcedures_thenSucceed() throws Exception {
		CreateProcessRequest cpr = new CreateProcessRequest();
		cpr.setScope("scope1");
		cpr.setWorkflow_code("GroovyWorkflow_A");
		Map<String, Object> globMap = new HashMap<String, Object>();
		cpr.setGlobal_vars(globMap);

		Response response = client.target(baseUrl).request().post(Entity.entity(cpr, MediaType.APPLICATION_JSON_TYPE));
		TstProcessResponse resBody = response.readEntity(TstProcessResponse.class);

		List<WorkflowMessage> messages = resBody.getMessages();
		assertEquals(2, messages.size());
	}
	
	@Test
	public void whenExtendedGroovyMessages_thenSucceed() throws Exception {
		CreateProcessRequest cpr = new CreateProcessRequest();
		cpr.setScope("scope1");
		cpr.setWorkflow_code("GroovyWorkflow_B");
		Map<String, Object> globMap = new HashMap<String, Object>();
		cpr.setGlobal_vars(globMap);

		Response response = client.target(baseUrl).request().post(Entity.entity(cpr, MediaType.APPLICATION_JSON_TYPE));
		TstProcessResponse resBody = response.readEntity(TstProcessResponse.class);

		List<WorkflowMessage> messages = resBody.getMessages();
		assertEquals(7, messages.size());
		assertTrue(messages.get(0).getCode().contains("root"));
		assertTrue(messages.get(1).getCode().contains("leaf"));
		assertTrue(messages.get(2).getCode().contains("other_leaf"));
	}
	
}
