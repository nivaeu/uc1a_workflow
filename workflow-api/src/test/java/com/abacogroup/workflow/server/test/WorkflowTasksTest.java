package com.abacogroup.workflow.server.test;

import static org.junit.Assert.assertNotNull;

/*-
 * #%L
 * workflow-api
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.JerseyClientBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import com.abacogroup.workflow.server.WorkflowApplication;
import com.abacogroup.workflow.server.WorkflowConfiguration;
import com.abacogroup.workflow.server.api.CreateProcessRequest;
import com.abacogroup.workflow.server.test.model.TstProcessResponse;

import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit5.DropwizardAppExtension;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;

@ExtendWith(DropwizardExtensionsSupport.class)
public class WorkflowTasksTest {

	private static DropwizardAppExtension<WorkflowConfiguration> EXT = 
		new DropwizardAppExtension<>( WorkflowApplication.class, ResourceHelpers.resourceFilePath("test_config_tasks.yml")) {
			protected JerseyClientBuilder clientBuilder() {
				JerseyClientBuilder builder = super.clientBuilder();
				builder.readTimeout(10, TimeUnit.SECONDS)
					.connectTimeout(2, TimeUnit.SECONDS);
				return builder;
			};
		};
	
	@Test
	public void whenWorkFlowJarIsUploaded_thenSuccessfullResponseFromTask() throws Exception {
		File file = new File("./src/test/resources/workflow/for-task/GroovyWorkflow_A_v1.jar");    
		final Response response = uploadFile(file, "jarFile.jar"); 
		assertTrue(response.getStatus() == 200);
	}
	
	@Test
	public void whenUpdateFile_thenSuccessfullResponseFromTask() throws Exception {    	
		File firstFile = new File("./src/test/resources/workflow/for-task/GroovyWorkflow_A_v1.jar");
		final Response responseFirstFile = uploadFile(firstFile, "jarFile.jar"); 
		assertTrue(responseFirstFile.getStatus() == 200);
		
		File updateFile = new File("./src/test/resources/workflow/for-task/GroovyWorkflow_A_v2.jar");    
		final Response responseUpdateFile = uploadFile(updateFile, "jarFile.jar"); 
		assertTrue(responseUpdateFile.getStatus() == 200);
	}
	
	@Test
	public void whenTransitionIsRunning_thenNewWorkflowUploadDontInterfere() throws Exception {    	
		Client client = EXT.client();

		List<TstProcessResponse> responses = Collections.synchronizedList(new ArrayList<>(2));
		Runnable r = () -> {
			CreateProcessRequest cpr = new CreateProcessRequest("ChangeWhileRunning", "scope");
			Response response = client.target("http://localhost:8080/workflow/api/v1/processes")
				.request().post(Entity.entity(cpr, MediaType.APPLICATION_JSON_TYPE));
		
			responses.add(response.readEntity(TstProcessResponse.class));				
		};
		
		File file = new File("./src/test/resources/workflow/for-task/ChangeWhileRunning-v1.jar");
		uploadFile(file, "ChangeWhileRunning.jar"); 
		
		Thread t1 = new Thread(r);
		t1.start();

		Thread.sleep(250);	// Sleep to wait for the first request to really reach the server...
		
		file = new File("./src/test/resources/workflow/for-task/ChangeWhileRunning-v2.jar");
		uploadFile(file, "ChangeWhileRunning.jar");
		
		Thread t2 = new Thread(r);
		t2.start();
		
		t1.join();
		t2.join();
		
		assertEquals(2, responses.size());
		
		/*
		 * The first version of the workflow sleeps for 2 seconds
		 * The second version of the workflow does not sleep
		 * We are expecting the second one to end before the first one
		 * and we are also expecting that the second version ended with the 
		 * initial metadata, so versions must be, in order: 002, 001
		 */
		
		responses.forEach(res -> {
			assertNotNull(res);	
			assertNotNull(res.getCurrent_node_metadata());
		});
		
		assertEquals("002", responses.get(0).getCurrent_node_metadata().get("version"));
		assertEquals("001", responses.get(1).getCurrent_node_metadata().get("version"));
	}

	private Response uploadFile(File file, String fileName) throws Exception
	{
		byte[] fileContent = null;		
		fileContent = Files.readAllBytes(file.toPath());

		String fileBase64 = Base64.getEncoder().encodeToString(fileContent);

		String queryParamFileName = "?fileName=" + fileName;
		Response res = EXT.client()
				.target(String.format("http://localhost:8081/tasks/upload-workflow" + queryParamFileName, EXT.getLocalPort()))
				.request()
				.post(Entity.entity(fileBase64, MediaType.TEXT_PLAIN), Response.class);
		
		Thread.sleep(100);	// Give a little margin to subsequent calls...
		
		return res;
	}

}
