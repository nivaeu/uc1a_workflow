package com.abacogroup.workflow.sdk.beans;

/*-
 * #%L
 * workflow-sdk
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.List;
import java.util.Map;

public interface ProcessData {

	/**
	 * Gets the current node of the workflow.
	 * @return the current node name
	 */
	String getCurrentNodeName();

	/**
	 * Gets the workflow messages.
	 * <p>
	 * You can add messages to the retuend list, these will be returned to the invoker of the current transition.
	 * </p>
	 * 
	 * @return The workflow messages.
	 */
	List<WorkflowMessage> getWorkflowMessages();

	/**
	 * Set the workflow messages.
	 * <p>
	 * The supplied messages will replace any messages already added; if you need to append messages, 
	 * use <code>{@link #getWorkflowMessages()}.add(message)</code>
	 * </p>
	 * @param workflowMessages the new messages
	 */
	void setWorkflowMessages(List<WorkflowMessage> workflowMessages);

	/**
	 * Gets the current process global state.
	 * @return the global state of the current process
	 */
	Map<String, ? super Object> getGlobalVars();

	/**
	 * @return the workflow id
	 */
	Integer getWorkflowId();

	/**
	 * @return the process id
	 */
	Long getProcessId();

	/**
	 * @return the current user id
	 */
	String getUserId();

	/**
	 * @return the current scope
	 */
	String getScope();

	/**
	 * @return the transition log id
	 */
	Long getLogTransitionId();

}
