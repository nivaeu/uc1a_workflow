package com.abacogroup.workflow.sdk.beans;

/*-
 * #%L
 * workflow-sdk
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

/**
 * A workflow message
 */
public class WorkflowMessage {
	
	private String code;
	private String description;
	private boolean success;
	
	public WorkflowMessage()
	{
	}
	
	public WorkflowMessage(String code, String description, boolean success)
	{
		this.code = code;
		this.description = description;
		this.success = success;
	}
	
	/**
	 * @return the message code
	 */
	public String getCode()
	{
		return code;
	}
	
	/**
	 * Sets the message code
	 * @param code the code
	 */
	public void setCode(String code)
	{
		this.code = code;
	}
	
	/**
	 * @return the message description
	 */
	public String getDescription()
	{
		return description;
	}
	
	/**
	 * Sets the message description
	 * @param description the description
	 */
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	/**
	 * @return true if the mesage represents a success information, false otherwise
	 */
	public boolean isSuccess()
	{
		return success;
	}
	
	/**
	 * Sets the success information status.
	 * @param success true if success
	 */
	public void setSuccess(boolean success)
	{
		this.success = success;
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + (success ? 1231 : 1237);
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WorkflowMessage other = (WorkflowMessage) obj;
		if (code == null)
		{
			if (other.code != null)
				return false;
		}
		else if (!code.equals(other.code))
			return false;
		if (description == null)
		{
			if (other.description != null)
				return false;
		}
		else if (!description.equals(other.description))
			return false;
		if (success != other.success)
			return false;
		return true;
	}
	
}
