package com.abacogroup.workflow.sdk.proc;

/*-
 * #%L
 * workflow-sdk
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import com.abacogroup.workflow.sdk.beans.ProcessData;

/**
 * Interface to be implemented by workflow procedures
 */
public interface Procedure {

	/**
	 * Executes the procedure
	 * 
	 * @param processData the data related to the current process (workflow instance)
	 * @throws Exception in case of any errors
	 */
	public void execute(ProcessData processData) throws Exception;
	
}
