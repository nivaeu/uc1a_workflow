# Workflow SDK

This maven module contains the workflow SDK classes; this is the only dependency you need in order to create a workflow that can be loaded and executed by the workflow server.
