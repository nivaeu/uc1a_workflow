package com.abacogroup.workflow.drools.service.impl;

/*-
 * #%L
 * workflow-drools
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.Date;

import org.kie.api.runtime.KieContainer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
class DroolsContainer {
	private @NonNull KieContainer kieContainer;
	private @NonNull Date lastModified;
}
