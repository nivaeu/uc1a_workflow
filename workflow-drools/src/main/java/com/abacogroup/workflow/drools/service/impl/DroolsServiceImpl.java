package com.abacogroup.workflow.drools.service.impl;

/*-
 * #%L
 * workflow-drools
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.kie.api.KieServices;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieRepository;
import org.kie.api.builder.ReleaseId;
import org.kie.api.definition.KiePackage;
import org.kie.api.definition.rule.Global;
import org.kie.api.io.Resource;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.io.ResourceFactory;

import com.abacogroup.workflow.drools.service.DroolsService;
import com.abacogroup.workflow.drools.service.FireRulesRequest;

public class DroolsServiceImpl implements DroolsService {

	private ClassLoader classLoader;

	public DroolsServiceImpl(ClassLoader classLoader) throws IOException {
		this.classLoader=classLoader;
	}

	@Override
	public Map<String, Object> getGlobalsMap(String ruleResource, Map<String, ?> globalVars) {
		StatelessKieSession kieSession = getKieSession(ruleResource);
		final Collection<KiePackage> kiePackages = kieSession.getKieBase().getKiePackages();
		Map<String, Object> globalsMap = new HashMap<String, Object>();
		// EXTRACT VARIABLE NAMES FROM RULE FILE
		for (KiePackage kiePackage : kiePackages) {
			final Collection<Global> globalVariables = kiePackage.getGlobalVariables();
			for (Global g : globalVariables) {
				Object globalVar = globalVars.get(g.getName());
				if (globalVar != null)
					globalsMap.put(g.getName(), globalVar);
			}
		}
		return globalsMap;
	}
	
	@Override
	public void fireRules(FireRulesRequest fireRulesRequest) {

		StatelessKieSession kieSession = getKieSession(fireRulesRequest.getRuleResource());

// 		DROOLS LOG SETUP
//		KieRuntimeLogger logger;
//		KieServices kieServices = KieServices.Factory.get();
//		kieSession.addEventListener(new DebugAgendaEventListener());
//		kieSession.addEventListener(new DebugRuleRuntimeEventListener());
//		logger = kieServices.getLoggers().newFileLogger(kieSession, "./rulesLog");
		
		if (fireRulesRequest.getVariableMap() != null) {
			fireRulesRequest.getVariableMap().forEach((k, v) -> kieSession.setGlobal(k, v));
		}
		
//		try {
			kieSession.execute(fireRulesRequest.getFactList());
//		} finally {
//			logger.close();
//		}

			
	}

	private StatelessKieSession getKieSession(String ruleResource) {
		DroolsCacheHolder cache = DroolsCacheHolder.getInstance();

		if (!cache.contains(ruleResource, classLoader)) {
			KieServices kieServices = KieServices.Factory.get();

			Resource resource = ResourceFactory.newClassPathResource(ruleResource, classLoader);

			KieFileSystem kieFileSystem = kieServices.newKieFileSystem().write(resource);
			kieServices.newKieBuilder(kieFileSystem, classLoader).buildAll();

			KieRepository kieRepository = kieServices.getRepository();
			ReleaseId krDefaultReleaseId = kieRepository.getDefaultReleaseId();
			KieContainer kieContainer = kieServices.newKieContainer(krDefaultReleaseId, classLoader);
			DroolsContainer ruleContainer = new DroolsContainer(kieContainer, new Date());
			cache.put(ruleResource, classLoader, ruleContainer);
		}
		
		return DroolsCacheHolder
			.getInstance()
			.get(ruleResource, classLoader)
			.getKieContainer().newStatelessKieSession();

	}

//	public String getDrlFromExcel(String excelFile) {
//		DecisionTableConfiguration configuration = KnowledgeBuilderFactory.newDecisionTableConfiguration();
//		configuration.setInputType(DecisionTableInputType.XLS);
//		Resource dt = ResourceFactory.newClassPathResource(excelFile, getClass());
//		DecisionTableProviderImpl decisionTableProvider = new DecisionTableProviderImpl();
//		String drl = decisionTableProvider.loadFromResource(dt, null);
//		return drl;
//	}
	
}
