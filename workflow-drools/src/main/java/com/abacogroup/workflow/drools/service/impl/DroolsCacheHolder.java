package com.abacogroup.workflow.drools.service.impl;

/*-
 * #%L
 * workflow-drools
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.util.concurrent.ConcurrentHashMap;

class DroolsCacheHolder {

	private static DroolsCacheHolder reference = null;

	private final ConcurrentHashMap<String, DroolsContainer> droolsContainerMap;

	private DroolsCacheHolder() {
		droolsContainerMap = new ConcurrentHashMap<String, DroolsContainer>();
	}

	public static DroolsCacheHolder getInstance() {
		if (reference == null) {
			synchronized (DroolsCacheHolder.class) {
				if (reference == null) {
					reference = new DroolsCacheHolder();
				}
			}
		}
		return reference;
	}

//	protected boolean isUpdatedRuleFile(File ruleFile) {
//		if (droolsContainerMap.get(ruleFile.getAbsolutePath()).getLastModified().before(new Date(ruleFile.lastModified()))) {
//			log.info("{} has been updated on {} ", ruleFile.getName(), new Date(ruleFile.lastModified()));
//			return true;
//		}
//		return false;
//	}

	public boolean contains(String ruleResource, ClassLoader classLoader)
	{
		return droolsContainerMap.containsKey(createCacheKey(ruleResource, classLoader));
	}

	public void put(String ruleResource, ClassLoader classLoader, DroolsContainer ruleContainer)
	{
		droolsContainerMap.put(createCacheKey(ruleResource, classLoader), ruleContainer);
	}

	public DroolsContainer get(String ruleResource, ClassLoader classLoader)
	{
		return droolsContainerMap.get(createCacheKey(ruleResource, classLoader));
	}

	private String createCacheKey(String ruleResource, ClassLoader classLoader) {
		return String.format("%s-%s", ruleResource, System.identityHashCode(classLoader));
	}

}
