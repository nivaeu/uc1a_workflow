package com.abacogroup.workflow.drools.service;

/*-
 * #%L
 * workflow-drools
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import java.io.IOException;
import java.util.Map;

import com.abacogroup.workflow.drools.service.impl.DroolsServiceImpl;

public interface DroolsService {

	static DroolsService create(ClassLoader classLoader) throws IOException {
		return new DroolsServiceImpl(classLoader);
	}
	
	void fireRules(FireRulesRequest fireRulesRequest);
	Map<String, Object> getGlobalsMap(String ruleResource, Map<String, ?> globalVars);

}
