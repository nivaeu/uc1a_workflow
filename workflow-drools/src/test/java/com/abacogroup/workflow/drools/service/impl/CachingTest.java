package com.abacogroup.workflow.drools.service.impl;

/*-
 * #%L
 * workflow-drools
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.RepetitionInfo;
import org.kie.api.runtime.KieContainer;

import com.abacogroup.workflow.drools.service.DroolsService;
import com.abacogroup.workflow.drools.service.FireRulesRequest;
import com.abacogroup.workflow.drools.test.model.Customer;
import com.abacogroup.workflow.drools.test.model.Customer.CustomerType;
import com.abacogroup.workflow.drools.test.model.EligibilityResult;

public class CachingTest {

	public static KieContainer kieContainer;

	@RepeatedTest(2)
	public void whenFireMoreTimes_thenUseDroolsCache(RepetitionInfo repetitionInfo) throws Exception {
		Customer customer1 = new Customer(CustomerType.INDIVIDUAL, 6);
		Customer customer2 = new Customer(CustomerType.INDIVIDUAL, 2);
		Customer customer3 = new Customer(CustomerType.BUSINESS, 51);
		ClassLoader classLoader = getClass().getClassLoader();
		String fileName = "Drools/Discount.xls";
		DroolsService ds;
		List<EligibilityResult> discountEligibilityList = new ArrayList<EligibilityResult>();
		ds = DroolsService.create(classLoader);
		List<Object> facts = new ArrayList<Object>();
		facts.add(customer1);
		facts.add(customer2);
		facts.add(customer3);
		Map<String, Object> varsMap = new HashMap<String, Object>();
		varsMap.put("eligibilityList", discountEligibilityList);
		FireRulesRequest request = new FireRulesRequest(fileName, facts, varsMap);
		ds.fireRules(request);
		if (repetitionInfo.getCurrentRepetition() == 2) {
			assertEquals(kieContainer, DroolsCacheHolder.getInstance().get(fileName, classLoader).getKieContainer());
		}else {
			kieContainer = DroolsCacheHolder.getInstance().get(fileName, classLoader).getKieContainer();
		}		
	}

}
