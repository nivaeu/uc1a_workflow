package com.abacogroup.workflow.drools.service;

/*-
 * #%L
 * workflow-drools
 * %%
 * Copyright (C) 2021 ABACO
 * %%
 * This file belongs to subproject seamless-claim of project NIVA (www.niva4cap.eu)
 *  All rights reserved
 * 
 *  Project and code is made available under the EU-PL v 1.2 license.
 * #L%
 */

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.kie.api.runtime.KieContainer;

import com.abacogroup.workflow.drools.test.model.Customer;
import com.abacogroup.workflow.drools.test.model.Customer.CustomerType;
import com.abacogroup.workflow.drools.test.model.EligibilityResult;

public class DroolsIntegrationTest {

	public static KieContainer kieContainer;

	@Test
	public void giveCustomerTypeAndAge_thenSetCorrectDiscountInFacts() throws Exception {
		Customer customer1 = new Customer(CustomerType.INDIVIDUAL, 6);
		Customer customer2 = new Customer(CustomerType.INDIVIDUAL, 2);
		Customer customer3 = new Customer(CustomerType.BUSINESS, 51);
		ClassLoader classLoader = getClass().getClassLoader();
		DroolsService ds;
		String fileName = "Drools/Discount.xls";
		List<EligibilityResult> discountEligibilityList = new ArrayList<EligibilityResult>();
		ds = DroolsService.create(classLoader);
		List<Object> facts = new ArrayList<Object>();
		facts.add(customer1);
		facts.add(customer2);
		facts.add(customer3);
		Map<String, Object> varsMap = new HashMap<String, Object>();
		varsMap.put("eligibilityList", discountEligibilityList);
		FireRulesRequest request = new FireRulesRequest(fileName, facts, varsMap);
		ds.fireRules(request);
		assertEquals(customer3.getDiscount(), 20);
		assertEquals(customer1.getDiscount(), 15);
		assertEquals(customer2.getDiscount(), 5);
	}

	@Test
	public void giveCustomerTypeAndAge_thenSetCorrectDiscountMessageInVars() throws Exception {
		Customer customer1 = new Customer(CustomerType.INDIVIDUAL, 6);
		Customer customer2 = new Customer(CustomerType.INDIVIDUAL, 2);
		Customer customer3 = new Customer(CustomerType.BUSINESS, 51);
		ClassLoader classLoader = getClass().getClassLoader();
		DroolsService ds;
		String fileName = "Drools/Discount.xls";
		List<EligibilityResult> discountEligibilityList = new ArrayList<EligibilityResult>();
		ds = DroolsService.create(classLoader);
		List<Object> facts = new ArrayList<Object>();
		facts.add(customer1);
		facts.add(customer2);
		facts.add(customer3);
		Map<String, Object> varsMap = new HashMap<String, Object>();
		varsMap.put("eligibilityList", discountEligibilityList);
		FireRulesRequest request = new FireRulesRequest(fileName, facts, varsMap);
		ds.fireRules(request);
		assertEquals(discountEligibilityList.get(0).getMessage(), "15% di sconto");
		assertEquals(discountEligibilityList.get(1).getMessage(), "5% di sconto");
		assertEquals(discountEligibilityList.get(2).getMessage(), "20% di sconto");
	}

}
