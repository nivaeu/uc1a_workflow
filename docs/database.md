# Database ER

```plantuml
hide circle
skinparam linetype ortho

entity "**wor_log_transition**" {
  + ""id"": //bigint [PK]//
  --
  *""user_id"": //character varying(100) //
  ""start_date"": //timestamp without time zone //
  ""end_date"": //timestamp without time zone //
  *""wor_process_id"": //integer [FK]//
  *""wor_workflow_transition_id"": //bigint [FK]//
  ""is_failed"": //numeric(1,0) //
  ""messages"": //text //
}

entity "**wor_process**" {
  + ""id"": //bigint [PK]//
  --
  ""node_current"": //character varying(30) //
  ""start_date"": //timestamp without time zone //
  *""wor_workflow_id"": //integer [FK]//
}

entity "**wor_process_data**" {
  + ""id"": //bigint [PK]//
  --
  *""variable"": //character varying(25) //
  *""value"": //text //
  *""wor_process_id"": //bigint [FK]//
}

entity "**wor_workflow**" {
  + ""id"": //integer [PK]//
  --
  ""code"": //character varying(50) //
  ""hash_md5"": //character varying(32) //
  ""version"": //character varying(10) //
  *""dt_delete"": //timestamp without time zone //
  *""dt_insert"": //timestamp without time zone //
}

entity "**wor_workflow_log**" {
  + ""id"": //integer [PK]//
  --
  ""code"": //character varying(50) [FK]//
  ""version"": //character varying(10) //
  *""dt_delete"": //timestamp without time zone //
  *""dt_insert"": //timestamp without time zone //
  *""json"": //text //
}

entity "**wor_workflow_node**" {
  + ""id"": //integer [PK]//
  --
  *""type"": //character varying(20) [FK]//
  ""action"": //character varying(200) //
  *""dt_insert"": //timestamp without time zone //
  ""dt_delete"": //timestamp without time zone //
  *""wor_workflow_id"": //integer [FK]//
  ""name"": //character varying(50) //
  ""metadata"": //text //
}

entity "**wor_workflow_node_types**" {
  + ""id"": //integer [PK]//
  --
  *""type_name"": //character varying(20) //
}

entity "**wor_workflow_rule_facts**" {
  + ""id"": //integer [PK]//
  --
  *""fact_name"": //character varying(25) //
  *""wor_workflow_node_id"": //integer [FK]//
  *""fact_type"": //character varying(120) //
  *""is_list"": //numeric(1,0) //
  *""dt_insert"": //timestamp without time zone //
}

entity "**wor_workflow_transition**" {
  + ""id"": //integer [PK]//
  --
  *""node_from"": //integer [FK]//
  *""node_to"": //integer [FK]//
  ""description"": //character varying(250) //
  ""is_default"": //smallint //
  *""wor_workflow_id"": //integer [FK]//
  *""dt_delete"": //timestamp without time zone //
  *""dt_insert"": //timestamp without time zone //
}

entity "**wor_workflow_transition_rule**" {
  + ""id"": //integer [PK]//
  --
  *""expression"": //text //
  *""wor_workflow_transition_id"": //integer [FK]//
  *""dt_insert"": //timestamp without time zone //
}

entity "**wor_workflow_transition_scope**" {
  + ""id"": //integer [PK]//
  --
  ""scope"": //character varying(25) //
  *""wor_workflow_transition_id"": //integer [FK]//
  *""dt_insert"": //timestamp without time zone //
}

entity "**wor_workflow_variable**" {
  + ""id"": //integer [PK]//
  --
  *""var_name"": //character varying(30) //
  *""var_type"": //character varying(200) //
  *""wor_workflow_id"": //integer [FK]//
  *""is_list"": //numeric(1,0) //
  *""fl_persist"": //numeric(1,0) //
  *""dt_delete"": //timestamp without time zone //
  *""dt_insert"": //timestamp without time zone //
}

"**wor_log_transition**"   }--  "**wor_process**"

"**wor_log_transition**"   }--  "**wor_workflow_transition**"

"**wor_process**"   }--  "**wor_workflow**"

"**wor_process_data**"   }--  "**wor_process**"

"**wor_workflow_log**"   }--  "**wor_workflow**"

"**wor_workflow_node**"   }--  "**wor_workflow_node_types**"

"**wor_workflow_node**"   }--  "**wor_workflow**"

"**wor_workflow_rule_facts**"   }--  "**wor_workflow_node**"

"**wor_workflow_transition**"   }--  "**wor_workflow_node**"

"**wor_workflow_transition**"   }--  "**wor_workflow_node**"

"**wor_workflow_transition**"   }--  "**wor_workflow**"

"**wor_workflow_transition_rule**"   }--  "**wor_workflow_transition**"

"**wor_workflow_transition_scope**"   }--  "**wor_workflow_transition**"

"**wor_workflow_variable**"   }--  "**wor_workflow**"
```
