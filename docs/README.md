# Glossary

* Workflow archive: a jar file containing a workflow definition and all its supporting code/rules.
* Workflow definition: a json file describing a workflow
* Workflow: a set of nodes connected using transitions
* Node: the node of a workflow
* Transition: a directed edge linking two nodes in a workflow
* variable definition: teh definitio ofa variable needed by the workflow
* Scope: a value used to filters availble transitions
* Procedure: a procedure that run some code
* Rule: a DROOLS ruleset
* Process: A `workflow instance`, it is a workflow and a running state that is isolated from all other processes
* Global state: the area containing the state of a process
* Variable: a variable with its value contained in the global state

# Workflow archive

A workflow archive is a jar file containing a workflow definition in the form of a file named 
`abaco-workflow.json` and also containing:
* compiled java classes needed for the workflow
* groovy scripts needed for the workflow
* DROOLS drl or xls files

# Workflow definition

The workflow definition is a json file, named `abaco-workflow.json` that follows the json schema in
`/workflow-api/src/main/resources/abaco-workflow-schema.json`.

Every workflow definition must have two atributes named code and version plus the followinf sections:

## nodes

```json
"nodes": [{
  "type": "start",
  "name": "start",
  "metadata": {}
}, {
  "type": "proc",
  "name": "NumberOfPixels",
  "metadata": {}
},
...
..., {
  "type": "end",
  "name": "noLUH",
  "metadata": {
    "message": "An example",
    "value": 10,
    "whatever": "testing"
  }
]
```

All workflow nodes must be present in this array, each node has the following attributes:
* type: the node type, one of: 
    * start: the start node, you can have only one start node per workflow definition
    * status: a status node, the process will stop when this kind of nodes are encountered, 
      waiting for user action
    * proc: a procedure
    * rule: a DROOLS rule
    * end: an end node, you can have many of these
* name: the node name, must be unique among all nodes
* metadata: a metadata object; when a process sops at a node with metadata set, the metadata will be returned to 
  the caller.

## procedures

```json
"procedures": [
  {
    "class_name": "com.abacogroup.workflow.server.test.proc.ProcTest",
    "node_name": "nodeP"
  },
  {
    "class_name" : "groovy/NumberOfPixels.groovy",
    "node_name" : "NumberOfPixels"
  },
  ...
  ...
]
```

All nodes of type `proc` must be present in this list; every entry has the following attributes:
* class_name: the java class or the groovy script implementing the SDK
  interface `com.abacogroup.workflow.sdk.proc.Procedure`
* node_name: the node name as set in the nodes section

## rules

```json
"rules": [
  {
    "drools_file": "string.xls",
    "node_name": "nodeR",
    "facts": [
      {
        "fact_name": "string",
        "fact_type": "java.lang.String",
        "is_list": 0
      }
    ]
  },
  ...
  ...
],
```

All nodes of type `rule` must be present in this list; every entry has the following attributes:
* drools_file: the DROOLS rulest rile, can be wither an xls or a drl file
* node_name: the node name as set in the nodes section
* facts: the list of facts to pass to DROOLS for evaluation, these are read from the gobal state variables:
    * fact_name: the fact variable name
    * fact_type: the java class type of the variable
    * is_list: 0 for regular objects, 1 if the variable is is a list of the specified `fact_type`

## transitions

```json
"transitions": [
  {
    "node_from": "nodeS",
    "node_to": "nodeP",
    "description": "string",
    "is_default": 0,
    "tran_rules": [
    ],
    "scopes": [
      { "scope": "string" }
    ]
  },
  {
    "node_from": "nodeP",
    "node_to": "nodeE",
    "is_default": 1,
    "description": "string",
    "tran_rules": [
      {
        "expression" : "variable.some.prop > 8"
      }
    ]
  },
  ...
  ...
],
```

This section contains all transition definitions for the workflow; each element has the following attributes:
* node_from: the transition starting node
* node_to: the transition ending node
* description: the transition description
* is_default: 0 indicating false, 1 indicating true
* tran_rules: a set of rules, rules are:
    * expressions that can use any global state variables, including nested properties using the dot notation
    * combined using the AND logical operator

## variables

```json
"variables": [
  {
    "name" : "documents",
    "type" : "my.package.Document",
    "is_list" : 1,
    "fl_persist" : 1
  },
  {
    "name" : "applicationId",
    "type" : "java.lang.Long",
    "is_list" : 0,
    "fl_persist" : 1
  },
  ...
  ...
]
```

This section contains all variables needed to create a process and initialize the global state area.
Defined variables will be considered mandatory for the process creation; callers must pass those or the
process creation will fail, throwing an exception.

Each element has the following attributes:
* name: the variable name
* type: the variable java class
* is_list: 0 indicating false, 1 indicating true; if 1 then a list of this type is expected
* fl_persist: 0 indicating false, 1 indicating true; if 1 then the variable will be persisted and will be
  available from all transitions, otherwise it will be available only at process creation time


# Server startup procedure

At server startup, the workflow archives are automatically loaded from the folder
defined in `config.yml` at the keys:
* `workflowPath`: for static workflows that cannot be updated during runtime
* `dynamicWorkflowPath`: for dynamic workflows that can be updated at runtime.

The server loads the `abaco-workflow.json` file and loads it into the database, updating the current workflow 
definition if any difference is found.

At this stage the workflow definition is checked against the json schema and it's consistency will be checked.

As a safeuard measure, you must change the workflow definition version if any changes are done to an already 
loaded workflow definition; to enforce this, the hash of the definition is computed and, if it differs from the 
one in the database and the version is not changed, an error will be thrown, otherwise the database will be 
updated.

During a workflow update, all processes using the workflow are scanned to detect at which node they are stopped,
if any of these nodes is missing from the new workflow, an error will be thrown and tje database will not be
updated.
